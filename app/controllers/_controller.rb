class Controller < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @ = .activo.page params[:page]
  end
  
  def new
    @ = .new
  end
  
  def edit
  end
  
  def create
    @ = .new(_params)
    if @area.save
      flash[:notice] = " añadida correctamente"
      redirect_to _path
    else
      flash[:alert] = "Hubo un error al añadir el "
      render "new"
    end
  end

  def update
    if @.update(_params)
      flash[:notice] = " modificada correctamente"
      redirect_to _path
    else
      flash[:alert] = "Hubo un error al añadir el "
      render "edit"
    end
  end
  
  def destroy
    @.activo = 0
    @.save
    flash[:notice] = " borrada correctamente"
    redirect_to _path
  end
  
  private
  def _params
    params.require(:).permit(:denominacion)
  end
  
  def set_
    @area = .activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa "
    redirect_to areas_path
  end
end
