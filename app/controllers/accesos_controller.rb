class AccesosController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_acceso, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @accesos = Acceso.activo.page params[:page]
  end
  
  def new
    @acceso = Acceso.new
  end
  
  def edit
  end
  
  def create
    @acceso = Acceso.new(area_params)
    if @acceso.save
      flash[:notice] = "Acceso añadido correctamente"
      redirect_to accesos_path
    else
      flash[:alert] = "Hubo un error al añadir el Acceso"
      render "new"
    end
  end

  def update
    if @acceso.update(area_params)
      flash[:notice] = "Acceso modificado correctamente"
      redirect_to accesos_path
    else
      flash[:alert] = "Hubo un error al añadir el Acceso"
      render "edit"
    end
  end
  
  def destroy
    @acceso.activo = 0
    @acceso.save
    flash[:notice] = "Acceso borrado correctamente"
    redirect_to accesos_path
  end
  
  private
  def area_params
    params.require(:acceso).permit(:denominacion)
  end
  
  def set_acceso
    @acceso = Acceso.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Acceso"
    redirect_to accesos_path
  end
end
