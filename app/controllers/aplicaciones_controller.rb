class AplicacionesController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_aplicacion, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @aplicaciones = Aplicacion.activo.page params[:page]
  end
  
  def new
    @aplicacion = Aplicacion.new
  end
  
  def edit
    
  end
  
  def create
    @aplicacion = Aplicacion.new(aplicacion_params)
    if @aplicacion.save
      flash[:notice] = "Aplicación añadida correctamente."
      redirect_to @aplicacion
    else
      flash[:alert] = "Hubo un error al añadir la Aplicación"
      render "new"
    end
  end
  
  def update
    if @aplicacion.update(aplicacion_params)
      flash[:notice] = "Aplicación modificada correctamente"
      redirect_to aplicaciones_path
    else
      flash[:alert] = "Hubo un error al añadir la Aplicación"
      render "edit"
    end
  end

  def show
  end
  
  def destroy
    @aplicacion.activo = 0
    @aplicacion.save
    flash[:notice] = "Aplicación borrada correctamente"
    redirect_to aplicaciones_path
  end
  
  private
  def aplicacion_params
    params.require(:aplicacion).permit(:nombre, :version, :referencia_externa,
    :descripcion, :area_id, :estado_aplicacion_id, :arquitectura_id, :tipo_aplicacion_id,
    :lenguaje_id, :version_lenguaje, :acceso_id, :seguridad_aplicacion_id, :suministro_id,
    :mantenimiento_id, :revisiones, :usuarios_nominales, :usuarios_concurrentes, :criticidad_id)
  end
  def set_aplicacion
    @aplicacion = Aplicacion.activo.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Aplicación"
    redirect_to aplicaciones_path
  end
end

