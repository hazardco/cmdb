class AreasController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_area, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @areas = Area.activo.page params[:page]
  end
  
  def new
    @area = Area.new
  end
  
  def edit
  end
  
  def create
    @area = Area.new(area_params)
    if @area.save
      flash[:notice] = "Área añadida correctamente"
      redirect_to areas_path
    else
      flash[:alert] = "Hubo un error al añadir el Área"
      render "new"
    end
  end

  def update
    if @area.update(area_params)
      flash[:notice] = "Área modificada correctamente"
      redirect_to areas_path
    else
      flash[:alert] = "Hubo un error al añadir el Área"
      render "edit"
    end
  end
  
  def destroy
    @area.activo = 0
    @area.save
    flash[:notice] = "Área borrada correctamente"
    redirect_to areas_path
  end
  
  private
  def area_params
    params.require(:area).permit(:denominacion)
  end
  
  def set_area
    @area = Area.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Área"
    redirect_to areas_path
  end
end
