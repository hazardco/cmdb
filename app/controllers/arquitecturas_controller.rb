class ArquitecturasController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_arquitectura, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @arquitecturas = Arquitectura.activo.page params[:page]
  end
  
  def new
    @arquitectura = Arquitectura.new
  end
  
  def edit
  end
  
  def create
    @arquitectura = Arquitectura.new(arquitectura_params)
    if @arquitectura.save
      flash[:notice] = "Arquitectura añadida correctamente"
      redirect_to arquitecturas_path
    else
      flash[:alert] = "Hubo un error al añadir la Arquitectura"
      render "new"
    end
  end

  def update
    if @arquitectura.update(arquitectura_params)
      flash[:notice] = "Arquitectura modificada correctamente"
      redirect_to arquitecturas_path
    else
      flash[:alert] = "Hubo un error al añadir la Arquitectura"
      render "edit"
    end
  end
  
  def destroy
    @arquitectura.activo = 0
    @arquitectura.save
    flash[:notice] = "Arquitectura borrada correctamente"
    redirect_to arquitecturas_path
  end
  
  private
  def arquitectura_params
    params.require(:arquitectura).permit(:denominacion)
  end
  
  def set_arquitectura
    @arquitectura = Arquitectura.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Arquitectura"
    redirect_to arquitecturas_path
  end
end
