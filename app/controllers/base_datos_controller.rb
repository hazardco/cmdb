class BaseDatosController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_base_dato, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @base_datos = BaseDato.activo.page params[:page]
  end
  
  def new
    @base_dato = BaseDato.new
  end
  
  def edit
    
  end
  
  def create
    @base_dato = BaseDato.new(base_dato_params)
    if @base_dato.save
      flash[:notice] = "Base de Datos añadida correctamente."
      redirect_to @aplicacion
    else
      flash[:alert] = "Hubo un error al añadir la Base de Datos"
      render "new"
    end
  end
  
  def update
    if @base_dato.update(base_dato_params)
      flash[:notice] = "Base de Datos modificada correctamente"
      redirect_to base_datos_path
    else
      flash[:alert] = "Hubo un error al añadir la Base de Datos"
      render "edit"
    end
  end

  def show
  end
  
  def destroy
    @base_dato.activo = 0
    @base_dato.save
    flash[:notice] = "Base de Datos borrada correctamente"
    redirect_to base_datos_path
  end
  
  private
  def base_dato_params
    params.require(:base_dato).permit(:nombre, :descripcion, :area_id, :estado_aplicacion_id, 
      :plataforma_bd_id, :tipo_bd_id, :version_plataforma, :seguridad_aplicacion_id, :criticidad_id,
      :suministro_id, :mantenimiento_id)
  end
  def set_base_dato
    @base_dato = BaseDato.activo.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Base de Datos"
    redirect_to base_datos_path
  end
end

