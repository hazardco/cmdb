class ContactosController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_servidor
  before_filter :set_contactos, only: [:edit, :update, :destroy]

  load_and_authorize_resource :except => [:create]

  def index
    @contactos = Contacto.activo.page params[:page]
  end
  
  def new
    @almacenamiento = @servidor.almacenamientos.build
  end
  
  def edit
    
  end
  
  def create
    @almacenamiento = @servidor.almacenamientos.build(almacenamiento_params)
    if @almacenamiento.save
      flash[:notice] = "Almacenamiento añadido correctamente."
      redirect_to servidor_almacenamientos_path
    else
      flash[:alert] = "Hubo un error al añadir el Almacenamiento"
      render "new"
    end
  end
  
  def update
    if @almacenamiento.update(almacenamiento_params)
      flash[:notice] = "Almacenamiento modificado correctamente."
      redirect_to servidor_almacenamientos_path
    else
      flash[:alert] = "Hubo un error al modificar el Almacenamiento"
      render action: "edit"
    end
  end
  
  def destroy
    @almacenamiento.activo = 0
    @almacenamiento.save
    flash[:notice] = "Almacenamiento borrado correctamente"
    redirect_to servidor_almacenamientos_path
  end
  
  private
  def almacenamiento_params
    params.require(:almacenamiento).permit(:denominacion, :tamanio, :fs, :tipo, :raid, :punto_montaje, :observaciones)
  end
  
  def set_almacenamiento
    @contacto = Contacto.find(params[:id])
  end
  
  def set_servidor
    @servidor = Servidor.find(params[:servidor_id])
  end
  
  #def set_almacenamiento
  #  @almacenamiento = Almacenamiento.activo.find(params[:id])
  #rescue ActiveRecord::RecordNotFound
  #  flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Almacenamiento"
  #  redirect_to servidores_almacenamientos_path
  #end
end