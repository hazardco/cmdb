class CriticidadesController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_criticidad, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @criticidades = Criticidad.activo.page params[:page]
  end
  
  def new
    @criticidad = Criticidad.new
  end
  
  def edit
  end
  
  def create
    @criticidad = Criticidad.new(criticidad_params)
    if @criticidad.save
      flash[:notice] = "Criticidad añadida correctamente"
      redirect_to criticidades_path
    else
      flash[:alert] = "Hubo un error al añadir la Criticidad"
      render "new"
    end
  end

  def update
    if @criticidad.update(criticidad_params)
      flash[:notice] = "Criticidad modificada correctamente"
      redirect_to criticidades_path
    else
      flash[:alert] = "Hubo un error al añadir la Criticidad"
      render "edit"
    end
  end
  
  def destroy
    @criticidad.activo = 0
    @criticidad.save
    flash[:notice] = "Criticidad borrada correctamente"
    redirect_to criticidades_path
  end
  
  private
  def criticidad_params
    params.require(:criticidad).permit(:denominacion)
  end
  
  def set_criticidad
    @criticidad = Criticidad.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Criticidad"
    redirect_to criticidades_path
  end
end
