class EntornosController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_entorno, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @entornos = Entorno.activo.page params[:page]
  end
  
  def new
    @entorno = Entorno.new
  end
  
  def edit
  end
  
  def create
    @entorno = Entorno.new(entorno_params)
    if @entorno.save
      flash[:notice] = "Entorno añadido correctamente"
      redirect_to entornos_path
    else
      flash[:alert] = "Hubo un error al añadir el Entorno"
      render "new"
    end
  end

  def update
    if @entorno.update(entorno_params)
      flash[:notice] = "Entorno modificado correctamente"
      redirect_to entornos_path
    else
      flash[:alert] = "Hubo un error al añadir el Entorno"
      render "edit"
    end
  end
  
  def destroy
    @entorno.activo = 0
    @entorno.save
    flash[:notice] = "Entorno borrado correctamente"
    redirect_to entornos_path
  end
  
  private
  def entorno_params
    params.require(:entorno).permit(:denominacion)
  end
  
  def set_entorno
    @entorno = Entorno.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Entorno"
    redirect_to entornos_path
  end
end
