class EstadoAplicacionesController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_estado_aplicacion, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @estado_aplicaciones = EstadoAplicacion.activo.page params[:page]
  end
  
  def new
    @estado_aplicacion = EstadoAplicacion.new
  end
  
  def edit
  end
  
  def create
    @estado_aplicacion = EstadoAplicacion.new(estado_aplicacion_params)
    if @estado_aplicacion.save
      flash[:notice] = "Estado de la Aplicación añadido correctamente"
      redirect_to estado_aplicaciones_path
    else
      flash[:alert] = "Hubo un error al añadir el Estado de la Aplicación"
      render "new"
    end
  end

  def update
    if @estado_aplicacion.update(estado_aplicacion_params)
      flash[:notice] = "Estado de la Aplicación modificado correctamente"
      redirect_to estado_aplicaciones_path
    else
      flash[:alert] = "Hubo un error al añadir el Estado de la Aplicación"
      render "edit"
    end
  end
  
  def destroy
    @estado_aplicacion.activo = 0
    @estado_aplicacion.save
    flash[:notice] = "Estado de la Aplicación borrado correctamente"
    redirect_to estado_aplicaciones_path
  end
  
  private
  def estado_aplicacion_params
    params.require(:estado_aplicacion).permit(:denominacion)
  end
  
  def set_estado_aplicacion
    @estado_aplicacion = EstadoAplicacion.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Estado de la Aplicación"
    redirect_to estado_aplicaciones_path
  end
end
