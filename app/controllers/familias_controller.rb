class FamiliasController < ApplicationController

  before_action :requiere_autenticacion!
  before_filter :set_familia, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @familias = Familia.activo.page params[:page]
  end
  
  def new
    @familia = Familia.new
  end
  
  def edit
  end
  
  def create
    @familia = Familia.new(familia_params)
    if @familia.save
      flash[:notice] = "Familia de SO añadida correctamente"
      redirect_to familias_path
    else
      flash[:alert] = "Hubo un error al añadir la Familia de SO"
      render "new"
    end
  end

  def update
    if @familia.update(familia_params)
      flash[:notice] = "Familia de SO modificada correctamente"
      redirect_to familias_path
    else
      flash[:alert] = "Hubo un error al añadir la Familia de SO"
      render "edit"
    end
  end
  
  def destroy
    @familia.activo = 0
    @familia.save
    flash[:notice] = "Familia de SO borrada correctamente"
    redirect_to familias_path
  end
  
  private
  def familia_params
    params.require(:familia).permit(:denominacion)
  end
  
  def set_familia
    @familia = Familia.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Familia de SO"
    redirect_to familias_path
  end
end
