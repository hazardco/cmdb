class IdiomasController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_idioma, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @idiomas = Idioma.activo.page params[:page]
  end
  
  def new
    @idioma = Idioma.new
  end
  
  def edit
  end
  
  def create
    @idioma = Idioma.new(idioma_params)
    if @idioma.save
      flash[:notice] = "Idioma añadido correctamente"
      redirect_to idiomas_path
    else
      flash[:alert] = "Hubo un error al añadir el Idioma"
      render "new"
    end
  end

  def update
    if @idioma.update(idioma_params)
      flash[:notice] = "Idioma modificado correctamente"
      redirect_to idiomas_path
    else
      flash[:alert] = "Hubo un error al añadir el Idioma"
      render "edit"
    end
  end
  
  def destroy
    @idioma.activo = 0
    @idioma.save
    flash[:notice] = "Idioma borrado correctamente"
    redirect_to idiomas_path
  end
  
  private
  def idioma_params
    params.require(:idioma).permit(:denominacion)
  end
  
  def set_idioma
    @idioma = Idioma.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Idioma"
    redirect_to idiomas_path
  end
end
