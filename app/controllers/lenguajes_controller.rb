class LenguajesController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_lenguaje, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @lenguajes = Lenguaje.activo.page params[:page]
  end
  
  def new
    @lenguaje = Lenguaje.new
  end
  
  def edit
  end
  
  def create
    @lenguaje = Lenguaje.new(lenguaje_params)
    if @lenguaje.save
      flash[:notice] = "Lenguaje añadido correctamente"
      redirect_to lenguajes_path
    else
      flash[:alert] = "Hubo un error al añadir el Lenguaje"
      render "new"
    end
  end

  def update
    if @lenguaje.update(lenguaje_params)
      flash[:notice] = "Lenguaje modificado correctamente"
      redirect_to lenguajes_path
    else
      flash[:alert] = "Hubo un error al añadir el Lenguaje"
      render "edit"
    end
  end
  
  def destroy
    @lenguaje.activo = 0
    @lenguaje.save
    flash[:notice] = "Lenguaje borrado correctamente"
    redirect_to lenguajes_path
  end
  
  private
  def lenguaje_params
    params.require(:lenguaje).permit(:denominacion)
  end
  
  def set_lenguaje
    @lenguaje = Lenguaje.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Lenguaje"
    redirect_to lenguajes_path
  end
end
