class MantenimientosController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_mantenimiento, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @mantenimientos = Mantenimiento.activo.page params[:page]
  end
  
  def new
    @mantenimiento = Mantenimiento.new
  end
  
  def edit
  end
  
  def create
    @mantenimiento = Mantenimiento.new(mantenimiento_params)
    if @mantenimiento.save
      flash[:notice] = "Mantenimiento añadido correctamente"
      redirect_to mantenimientos_path
    else
      flash[:alert] = "Hubo un error al añadir el Mantenimiento"
      render "new"
    end
  end

  def update
    if @mantenimiento.update(mantenimiento_params)
      flash[:notice] = "Mantenimiento modificado correctamente"
      redirect_to mantenimientos_path
    else
      flash[:alert] = "Hubo un error al añadir el Mantenimiento"
      render "edit"
    end
  end
  
  def destroy
    @mantenimiento.activo = 0
    @mantenimiento.save
    flash[:notice] = "Mantenimiento borrado correctamente"
    redirect_to mantenimientos_path
  end
  
  private
  def mantenimiento_params
    params.require(:mantenimiento).permit(:denominacion)
  end
  
  def set_mantenimiento
    @mantenimiento = Mantenimiento.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Mantenimiento"
    redirect_to mantenimientos_path
  end
end