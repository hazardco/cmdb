class PlataformaBdsController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_plataforma_bd, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @plataforma_bds = PlataformaBd.activo.page params[:page]
  end
  
  def new
    @plataforma_bd = PlataformaBd.new
  end
  
  def edit
  end
  
  def create
    @plataforma_bd = PlataformaBd.new(plataforma_bd_params)
    if @plataforma_bd.save
      flash[:notice] = "Plataforma de Base de Datos añadida correctamente"
      redirect_to plataforma_bds_path
    else
      flash[:alert] = "Hubo un error al añadir la Plataforma de Base de Datos"
      render "new"
    end
  end

  def update
    if @plataforma_bd.update(plataforma_bd_params)
      flash[:notice] = "Plataforma de Base de Datos modificada correctamente"
      redirect_to plataforma_bds_path
    else
      flash[:alert] = "Hubo un error al modificar la Plataforma de Base de Datos"
      render "edit"
    end
  end
  
  def destroy
    @plataforma_bd.activo = 0
    @plataforma_bd.save
    flash[:notice] = "Plataforma de Base de Datos borrada correctamente"
    redirect_to plataforma_bds_path
  end
  
  private
  def plataforma_bd_params
    params.require(:plataforma_bd).permit(:denominacion)
  end
  
  def set_plataforma_bd
    @plataforma_bd = PlataformaBd.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Plataforma de Base de Datos"
    redirect_to plataforma_bds_path
  end
end
