class PlataformasController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_plataforma, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @plataformas = Plataforma.activo.page params[:page]
  end
  
  def new
    @plataforma = Plataforma.new
  end
  
  def edit
  end
  
  def create
    @plataforma = Plataforma.new(plataforma_params)
    if @plataforma.save
      flash[:notice] = "Plataforma añadida correctamente"
      redirect_to plataformas_path
    else
      flash[:alert] = "Hubo un error al añadir la Plataforma"
      render "new"
    end
  end

  def update
    if @plataforma.update(plataforma_params)
      flash[:notice] = "Plataforma modificada correctamente"
      redirect_to plataformas_path
    else
      flash[:alert] = "Hubo un error al añadir la Plataforma"
      render "edit"
    end
  end
  
  def destroy
    @plataforma.activo = 0
    @plataforma.save
    flash[:notice] = "Plataforma borrada correctamente"
    redirect_to plataformas_path
  end
  
  private
  def plataforma_params
    params.require(:plataforma).permit(:denominacion)
  end
  
  def set_plataforma
    @plataforma = Plataforma.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Plataforma"
    redirect_to plataformas_path
  end
end
