class RolServidoresController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_rol_servidor, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @rol_servidores = RolServidor.activo.page params[:page]
  end
  
  def new
    @rol_servidor = RolServidor.new
  end
  
  def edit
  end
  
  def create
    @rol_servidor = RolServidor.new(rol_servidor_params)
    if @rol_servidor.save
      flash[:notice] = "Rol de Servidor añadido correctamente"
      redirect_to rol_servidores_path
    else
      flash[:alert] = "Hubo un error al añadir el Rol de Servidor"
      render "new"
    end
  end

  def update
    if @rol_servidor.update(rol_servidor_params)
      flash[:notice] = "Rol de Servidor modificado correctamente"
      redirect_to rol_servidores_path
    else
      flash[:alert] = "Hubo un error al añadir el Rol de Servidor"
      render "edit"
    end
  end
  
  def destroy
    @rol_servidor.activo = 0
    @rol_servidor.save
    flash[:notice] = "Rol de Servidor borrado correctamente"
    redirect_to rol_servidores_path
  end
  
  private
  def rol_servidor_params
    params.require(:rol_servidor).permit(:denominacion)
  end
  
  def set_rol_servidor
    @rol_servidor = RolServidor.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Rol de Servidor"
    redirect_to rol_servidores_path
  end
end
