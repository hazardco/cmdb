class SeguridadAplicacionesController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_seguridad_aplicacion, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @seguridad_aplicaciones = SeguridadAplicacion.activo.page params[:page]
  end
  
  def new
    @seguridad_aplicacion = SeguridadAplicacion.new
  end
  
  def edit
  end
  
  def create
    @seguridad_aplicacion = SeguridadAplicacion.new(seguridad_aplicacion_params)
    if @seguridad_aplicacion.save
      flash[:notice] = "Seguridad de la Aplicación añadida correctamente"
      redirect_to seguridad_aplicaciones_path
    else
      flash[:alert] = "Hubo un error al añadir la Seguridad de la Aplicación"
      render "new"
    end
  end

  def update
    if @seguridad_aplicacion.update(seguridad_aplicacion_params)
      flash[:notice] = "Seguridad de Aplicación modificada correctamente"
      redirect_to seguridad_aplicaciones_path
    else
      flash[:alert] = "Hubo un error al añadir la Seguridad de Aplicación"
      render "edit"
    end
  end
  
  def destroy
    @seguridad_aplicacion.activo = 0
    @seguridad_aplicacion.save
    flash[:notice] = "Seguridad de Aplicación borrada correctamente"
    redirect_to seguridad_aplicaciones_path
  end
  
  private
  def seguridad_aplicacion_params
    params.require(:seguridad_aplicacion).permit(:denominacion)
  end
  
  def set_seguridad_aplicacion
    @seguridad_aplicacion = SeguridadAplicacion.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Seguridad de Aplicación"
    redirect_to seguridad_aplicaciones_path
  end
end
