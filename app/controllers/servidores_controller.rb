class ServidoresController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_servidor, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @servidores = Servidor.activo.page params[:page]
  end
  
  def new
    @servidor = Servidor.new
  end
  
  def edit
    
  end
  
  def create
    @servidor = Servidor.new(servidor_params)
    if @servidor.save
      flash[:notice] = "Servidor añadido correctamente."
      redirect_to @servidor
    else
      flash[:alert] = "Hubo un error al añadir el Servidor"
      render "new"
    end
  end
  
  def update
    if @servidor.update(servidor_params)
      flash[:notice] = "Servidor modificado correctamente"
      redirect_to servidores_path
    else
      flash[:alert] = "Hubo un error al añadir el Servidor"
      render "edit"
    end
  end

  def show
  end
  
  def destroy
    @servidor.activo = 0
    @servidor.save
    flash[:notice] = "Servidor borrado correctamente"
    redirect_to servidores_path
  end
  
  def actualizar_plataforma
    @tipo_up_id = params[:servidor_tipo_up_id]
    respond_to do |format|
      format.js
    end
  end
  
  private
  def servidor_params
    params.require(:servidor).permit(:hostname, :fqdn, :tipo_up_id, :plataforma_id, 
      :virtual, :familia_id, :sistema_operativo, :idioma_id, :tipo_licencia_id,
      :numero_licencias, :rol_servidor_id, :rol_secundario_id, :entorno_id, 
      :criticidad_id, :monitorizacion, :descripcion, :memoria, :procesadores, 
      :tipo_procesador, :ubicacion_id, :rack, :marca, :modelo, :n_serie, :tamanio, :conexiones, 
      :tipo_conexiones, :intensidad, :potencia_maxima)
  end
  def set_servidor
    @servidor = Servidor.activo.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Servidor"
    redirect_to servidores_path
  end
end

