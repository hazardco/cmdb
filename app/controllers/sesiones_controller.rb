class SesionesController < ApplicationController
  layout 'login'

  def new
  end
  
  def create
    usuario = Usuario.where(:usuario => params[:login][:usuario]).first
    if usuario && usuario.authenticate(params[:login][:password])
      session[:usuario_id] = usuario.id
      flash[:notice] = "Login correcto"
      redirect_to dashboard_path
    else
      flash[:alert] = "Login incorrecto"
      render :new
    end 
  end
  
  def destroy
    session[:usuario_id] = nil
    flash[:notice] = "Ha salido correctamente de la aplicación"
    redirect_to login_path
  end
end
