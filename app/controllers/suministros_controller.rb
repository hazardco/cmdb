class SuministrosController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_suministro, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @suministros = Suministro.activo.page params[:page]
  end
  
  def new
    @lenguaje = Suministro.new
  end
  
  def edit
  end
  
  def create
    @suministro = Suministro.new(suministro_params)
    if @suministro.save
      flash[:notice] = "Suministro añadido correctamente"
      redirect_to suministros_path
    else
      flash[:alert] = "Hubo un error al añadir el Suministro"
      render "new"
    end
  end

  def update
    if @suministro.update(suministro_params)
      flash[:notice] = "Suministro modificado correctamente"
      redirect_to suministros_path
    else
      flash[:alert] = "Hubo un error al añadir el Suministro"
      render "edit"
    end
  end
  
  def destroy
    @suministro.activo = 0
    @suministro.save
    flash[:notice] = "Suministro borrado correctamente"
    redirect_to suministros_path
  end
  
  private
  def suministro_params
    params.require(:suministro).permit(:denominacion)
  end
  
  def set_suministro
    @suministro = Suministro.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Suministro"
    redirect_to suministros_path
  end
end
