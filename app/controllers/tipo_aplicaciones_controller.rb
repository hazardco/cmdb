class TipoAplicacionesController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_tipo_aplicacion, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @tipo_aplicaciones = TipoAplicacion.activo.page params[:page]
  end
  
  def new
    @tipo_aplicacion = TipoAplicacion.new
  end
  
  def edit
  end
  
  def create
    @tipo_aplicacion = TipoAplicacion.new(tipo_aplicacion_params)
    if @tipo_aplicacion.save
      flash[:notice] = "Tipo de la Aplicación añadido correctamente"
      redirect_to tipo_aplicaciones_path
    else
      flash[:alert] = "Hubo un error al añadir el Tipo de la Aplicación"
      render "new"
    end
  end

  def update
    if @tipo_aplicacion.update(tipo_aplicacion_params)
      flash[:notice] = "Tipo de la Aplicación modificado correctamente"
      redirect_to tipo_aplicaciones_path
    else
      flash[:alert] = "Hubo un error al añadir el Tipo de la Aplicación"
      render "edit"
    end
  end
  
  def destroy
    @tipo_aplicacion.activo = 0
    @tipo_aplicacion.save
    flash[:notice] = "Tipo de la Aplicación borrado correctamente"
    redirect_to tipo_aplicaciones_path
  end
  
  private
  def tipo_aplicacion_params
    params.require(:tipo_aplicacion).permit(:denominacion)
  end
  
  def set_tipo_aplicacion
    @tipo_aplicacion = TipoAplicacion.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Tipo de la Aplicación"
    redirect_to tipo_aplicaciones_path
  end
end
