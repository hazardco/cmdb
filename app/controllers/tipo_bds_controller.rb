class TipoBdsController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_tipo_bd, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @tipo_bds = TipoBd.activo.page params[:page]
  end
  
  def new
    @tipo_bd = TipoBd.new
  end
  
  def edit
  end
  
  def create
    @tipo_bd = TipoBd.new(tipo_bd_params)
    if @tipo_bd.save
      flash[:notice] = "Tipo de Base de Datos añadida correctamente"
      redirect_to tipo_bds_path
    else
      flash[:alert] = "Hubo un error al añadir el Tipo de Base de Datos"
      render "new"
    end
  end

  def update
    if @tipo_bd.update(tipo_bd_params)
      flash[:notice] = "Tipo de Base de Datos modificada correctamente"
      redirect_to tipo_bds_path
    else
      flash[:alert] = "Hubo un error al modificar el Tipo de Base de Datos"
      render "edit"
    end
  end
  
  def destroy
    @tipo_bd.activo = 0
    @tipo_bd.save
    flash[:notice] = "Tipo de Base de Datos borrada correctamente"
    redirect_to tipo_bds_path
  end
  
  private
  def tipo_bd_params
    params.require(:tipo_bd).permit(:denominacion)
  end
  
  def set_tipo_bd
    @tipo_bd = TipoBd.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Tipo de Base de Datos"
    redirect_to tipo_bds_path
  end
end
