class TipoLicenciasController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_tipo_licencia, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @tipo_licencias = TipoLicencia.activo.page params[:page]
  end
  
  def new
    @tipo_licencia = TipoLicencia.new
  end
  
  def edit
  end
  
  def create
    @tipo_licencia = TipoLicencia.new(tipo_licencia_params)
    if @tipo_licencia.save
      flash[:notice] = "Tipo de Licencia añadido correctamente"
      redirect_to tipo_licencias_path
    else
      flash[:alert] = "Hubo un error al añadir el Tipo de Licencia"
      render "new"
    end
  end

  def update
    if @tipo_licencia.update(tipo_licencia_params)
      flash[:notice] = "Tipo de Licencia modificado correctamente"
      redirect_to tipo_licencias_path
    else
      flash[:alert] = "Hubo un error al añadir el Tipo de Licencia"
      render "edit"
    end
  end
  
  def destroy
    @tipo_licencia.activo = 0
    @tipo_licencia.save
    flash[:notice] = "Tipo de Licencia borrado correctamente"
    redirect_to tipo_licencias_path
  end
  
  private
  def tipo_licencia_params
    params.require(:tipo_licencia).permit(:denominacion)
  end
  
  def set_tipo_licencia
    @tipo_licencia = TipoLicencia.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Tipo de Licencia"
    redirect_to tipo_licencias_path
  end
end
