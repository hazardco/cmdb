class TipoUpsController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_tipo_up, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @tipo_ups = TipoUp.activo.page params[:page]
  end
  
  def new
    @tipo_up = TipoUp.new
  end
  
  def edit
  end
  
  def create
    @tipo_up = TipoUp.new(tipo_up_params)
    if @tipo_up.save
      flash[:notice] = "Tipo de UP añadido correctamente"
      redirect_to tipo_ups_path
    else
      flash[:alert] = "Hubo un error al añadir el Tipo de UP"
      render "new"
    end
  end

  def update
    if @tipo_up.update(tipo_up_params)
      flash[:notice] = "Tipo de UP modificado correctamente"
      redirect_to tipo_ups_path
    else
      flash[:alert] = "Hubo un error al añadir el Tipo de UP"
      render "edit"
    end
  end
  
  def destroy
    @tipo_up.activo = 0
    @tipo_up.save
    flash[:notice] = "Tipo de UP borrado correctamente"
    redirect_to tipo_ups_path
  end
  
  private
  def tipo_up_params
    params.require(:tipo_up).permit(:denominacion)
  end
  
  def set_tipo_up
    @tipo_up = TipoUp.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar ese Tipo de Unidad de Proceso"
    redirect_to tipo_ups_path
  end
end
