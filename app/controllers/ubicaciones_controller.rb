class UbicacionesController < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_ubicacion, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @ubicaciones = Ubicacion.activo.page params[:page]
  end
  
  def new
    @ubicacion = Ubicacion.new
  end
  
  def edit
  end
  
  def create
    @ubicacion = Ubicacion.new(ubicacion_params)
    if @ubicacion.save
      flash[:notice] = "Ubicación añadida correctamente"
      redirect_to ubicaciones_path
    else
      flash[:alert] = "Hubo un error al añadir la Ubicación"
      render "new"
    end
  end

  def update
    if @ubicacion.update(ubicacion_params)
      flash[:notice] = "Ubicación modificada correctamente"
      redirect_to ubicaciones_path
    else
      flash[:alert] = "Hubo un error al añadir la Ubicación"
      render "edit"
    end
  end
  
  def destroy
    @ubicacion.activo = 0
    @ubicacion.save
    flash[:notice] = "Ubicación borrado correctamente"
    redirect_to entornos_path
  end
  
  private
  def ubicacion_params
    params.require(:ubicacion).permit(:denominacion)
  end
  
  def set_ubicacion
    @ubicacion = Ubicacion.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa Ubicación"
    redirect_to ubicaciones_path
  end
end