class Aplicacion < ActiveRecord::Base
paginates_per 10
  validates :nombre, presence: true

  scope :activo, -> { where(activo: 1) }

  belongs_to :area
  belongs_to :arquitectura
  belongs_to :lenguaje
  belongs_to :acceso
  belongs_to :suministro
  belongs_to :estado_aplicacion
  belongs_to :tipo_aplicacion
  belongs_to :seguridad_aplicacion
  belongs_to :mantenimiento
  belongs_to :criticidad
end
