class BaseDato < ActiveRecord::Base
paginates_per 10
  validates :denominacion, presence: true

  scope :activo, -> { where(activo: 1) }

  belongs_to :area
  belongs_to :estado_aplicacion
  belongs_to :plataforma_bd
  belongs_to :tipo_bd
  belongs_to :seguridad_aplicacion
  belongs_to :criticidad
  belongs_to :suministro
  belongs_to :mantenimiento
end
