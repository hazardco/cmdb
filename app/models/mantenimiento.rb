class Mantenimiento < ActiveRecord::Base
paginates_per 10
  validates :denominacion, presence: true

  scope :activo, -> { where(activo: 1) }
end
