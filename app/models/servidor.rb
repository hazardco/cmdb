class Servidor < ActiveRecord::Base
has_many :almacenamientos

  paginates_per 10

  belongs_to :tipo_up
  belongs_to :plataforma
  belongs_to :familia
  belongs_to :idioma
  belongs_to :tipo_licencia
  belongs_to :entorno
  belongs_to :criticidad
  belongs_to :rol_servidor
  belongs_to :ubicacion
  
  validates :descripcion, presence: true

  scope :activo, -> { where(activo: 1) }

end
