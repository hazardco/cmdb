# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end

# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end
ActiveSupport::Inflector.inflections(:en) do |inflect|
  inflect.irregular 'servidor', 'servidores'
  inflect.irregular 'familia', 'familias'
  inflect.irregular 'error', 'errores'
  inflect.irregular 'plataforma', 'plataformas'
  inflect.irregular 'idioma', 'idiomas'
  inflect.irregular 'tipo_up', 'tipo_ups'  
  inflect.irregular 'tipo_licencia', 'tipo_licencias'
  inflect.irregular 'ubicacion', 'ubicaciones'
  inflect.irregular 'rol_servidor', 'rol_servidores'
  inflect.irregular 'criticidad', 'criticidades'
  inflect.irregular 'usuario', 'usuarios'
  inflect.irregular 'sesion', 'sesiones'
  inflect.irregular 'area', 'areas'
  inflect.irregular 'arquitectura', 'arquitecturas'
  inflect.irregular 'lenguaje', 'lenguajes'
  inflect.irregular 'acceso', 'accesos'
  inflect.irregular 'suministro', 'suministros'
  inflect.irregular 'estado_aplicacion', 'estado_aplicaciones'
  inflect.irregular 'tipo_aplicacion', 'tipo_aplicaciones'
  inflect.irregular 'seguridad_aplicacion', 'seguridad_aplicaciones'
  inflect.irregular 'mantenimiento', 'mantenimientos'
  inflect.irregular 'aplicacion', 'aplicaciones'
  inflect.irregular 'plataforma_bd', 'plataforma_bds'
  inflect.irregular 'tipo_bd', 'tipo_bds'  
  inflect.irregular 'base_dato', 'bases_datos'
  inflect.irregular 'almacenamiento', 'almacenamientos'
  inflect.irregular 'contacto', 'contactos'
end