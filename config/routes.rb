Cmdb::Application.routes.draw do

  resources :pruebas

  get "dashboard/", to: "dashboard#index"
  
  get 'servidores/actualizar_plataforma' => 'servidores#actualizar_plataforma'
  
  resources :servidores do
    
    resources :almacenamientos
    
    resources :contactos
    
  end
  
  resources :usuarios
  
  resources :aplicaciones
  
  resources :base_datos
  
  get "/login", to: "sesiones#new"
  
  post "/login", to: "sesiones#create"
  
  delete "/login", to: "sesiones#destroy"
  
  #TABLAS DEL CMDB SISTEMAS
  
  resources :familias
  
  resources :entornos
  
  resources :idiomas
  
  resources :tipo_ups
  
  resources :tipo_licencias
  
  resources :plataformas
  
  resources :ubicaciones
  
  resources :rol_servidores
  
  resources :criticidades
  
  #TABLAS DEL CMDB DESARROLLO
  
  resources :areas
  
  resources :arquitecturas
  
  resources :lenguajes
  
  resources :accesos
  
  resources :suministros
  
  resources :estado_aplicaciones
  
  resources :tipo_aplicaciones
  
  resources :seguridad_aplicaciones
  
  resources :mantenimientos
  
  resources :plataforma_bds
  
  resources :tipo_bds
  
  root 'dashboard#index'
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
