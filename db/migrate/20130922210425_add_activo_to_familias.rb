class AddActivoToFamilias < ActiveRecord::Migration
  def change
    add_column :familias, :activo, :integer, default: 1
  end
end
