class CreateTipoUps < ActiveRecord::Migration
  def change
    create_table :tipo_ups do |t|
      t.string :denominacion
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
