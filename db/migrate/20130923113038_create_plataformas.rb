class CreatePlataformas < ActiveRecord::Migration
  def change
    create_table :plataformas do |t|
      t.string :denominacion
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
