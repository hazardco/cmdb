class CreateTipoLicencias < ActiveRecord::Migration
  def change
    create_table :tipo_licencias do |t|
      t.string :denominacion
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
