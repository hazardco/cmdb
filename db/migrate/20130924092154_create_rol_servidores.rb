class CreateRolServidores < ActiveRecord::Migration
  def change
    create_table :rol_servidores do |t|
      t.string :denominacion
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
