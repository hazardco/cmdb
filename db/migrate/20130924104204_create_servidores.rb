class CreateServidores < ActiveRecord::Migration
  def change
    create_table :servidores do |t|
      t.string :hostname
      t.string :fqdn
      t.references :tipo_up, index: true
      t.references :plataforma, index: true
      t.string :virtual
      t.references :familia, index: true
      t.string :sistema_operativo
      t.references :idioma, index: true
      t.references :tipo_licencia, index: true
      t.integer :numero_licencias
      t.references :entorno, index: true
      t.references :criticidad, index: true
      t.boolean :monitorizacion
      t.references :rol_servidor, index: true
      t.string :descripcion
      t.integer :memoria
      t.integer :procesadores
      t.string :tipo_procesador
      t.references :ubicacion, index: true
      t.string :rack
      t.string :marca
      t.string :modelo
      t.string :n_serie
      t.integer :tamanio
      t.string :conexiones
      t.string :tipo_conexiones
      t.string :intensidad
      t.string :potencia_maxima
      t.integer :activo, default: 1
      t.timestamps
    end
  end
end
