class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :nombre
      t.string :apellidos
      t.string :correo
      t.string :password_digest
      t.string :usuario
      t.integer :activo, default: 1      

      t.timestamps
    end
  end
end
