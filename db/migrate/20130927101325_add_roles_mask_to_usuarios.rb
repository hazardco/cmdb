class AddRolesMaskToUsuarios < ActiveRecord::Migration
  def change
    add_column :usuarios, :roles_mask, :integer, default:0
  end
end
