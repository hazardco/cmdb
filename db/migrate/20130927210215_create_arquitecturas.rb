class CreateArquitecturas < ActiveRecord::Migration
  def change
    create_table :arquitecturas do |t|
      t.string :denominacion
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
