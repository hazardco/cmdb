class CreateLenguajes < ActiveRecord::Migration
  def change
    create_table :lenguajes do |t|
      t.string :denominacion
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
