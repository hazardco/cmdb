class CreateAccesos < ActiveRecord::Migration
  def change
    create_table :accesos do |t|
      t.string :denominacion
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
