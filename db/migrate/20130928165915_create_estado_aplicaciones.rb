class CreateEstadoAplicaciones < ActiveRecord::Migration
  def change
    create_table :estado_aplicaciones do |t|
      t.string :denominacion
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
