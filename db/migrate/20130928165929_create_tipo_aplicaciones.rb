class CreateTipoAplicaciones < ActiveRecord::Migration
  def change
    create_table :tipo_aplicaciones do |t|
      t.string :denominacion
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
