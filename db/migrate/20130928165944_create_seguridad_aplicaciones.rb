class CreateSeguridadAplicaciones < ActiveRecord::Migration
  def change
    create_table :seguridad_aplicaciones do |t|
      t.string :denominacion
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
