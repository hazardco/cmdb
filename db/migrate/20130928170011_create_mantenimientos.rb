class CreateMantenimientos < ActiveRecord::Migration
  def change
    create_table :mantenimientos do |t|
      t.string :denominacion
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
