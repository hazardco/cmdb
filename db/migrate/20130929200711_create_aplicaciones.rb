class CreateAplicaciones < ActiveRecord::Migration
  def change
    create_table :aplicaciones do |t|
      t.string :nombre
      t.string :version
      t.string :referencia_externa
      t.string :descripcion
      t.references :area, index: true
      t.references :arquitectura, index: true
      t.references :lenguaje, index: true
      t.references :acceso, index: true
      t.references :suministro, index: true
      t.references :estado_aplicacion, index: true
      t.references :tipo_aplicacion, index: true
      t.string :version_lenguaje
      t.references :seguridad_aplicacion, index: true
      t.references :mantenimiento, index: true
      t.integer :revisiones
      t.integer :usuarios_nominales
      t.integer :usuarios_concurrentes
      t.references :criticidad, index: true
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
