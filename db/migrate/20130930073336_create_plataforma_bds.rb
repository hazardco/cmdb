class CreatePlataformaBds < ActiveRecord::Migration
  def change
    create_table :plataforma_bds do |t|
      t.string :denominacion
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
