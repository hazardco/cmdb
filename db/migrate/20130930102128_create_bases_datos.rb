class CreateBasesDatos < ActiveRecord::Migration
  def change
    create_table :bases_datos do |t|
      t.string :nombre
      t.string :descripcion
      t.references :area, index: true
      t.references :estado_aplicacion, index: true
      t.references :plataforma_bd, index: true
      t.references :tipo_bd, index: true
      t.string :version_plataforma
      t.references :seguridad_aplicacion, index: true
      t.references :criticidad, index: true
      t.references :suministro, index: true
      t.references :mantenimiento, index: true
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
