class CreateAlmacenamientos < ActiveRecord::Migration
  def change
    create_table :almacenamientos do |t|
      t.string :denominacion
      t.string :tamanio
      t.string :fs
      t.string :tipo
      t.string :raid
      t.string :punto_montaje
      t.string :observaciones
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
