class AddServidorIdToAlmacenamientos < ActiveRecord::Migration
  def change
    add_reference :almacenamientos, :servidor, index: true
  end
end
