class CreateContactos < ActiveRecord::Migration
  def change
    create_table :contactos do |t|
      t.string :nombre
      t.string :apellidos
      t.string :correo
      t.string :telefono
      t.integer :activo, default: 1

      t.timestamps
    end
  end
end
