# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131003113624) do

  create_table "accesos", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "almacenamientos", force: true do |t|
    t.string   "denominacion"
    t.string   "tamanio"
    t.string   "fs"
    t.string   "tipo"
    t.string   "raid"
    t.string   "punto_montaje"
    t.string   "observaciones"
    t.integer  "activo",        default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "servidor_id"
  end

  add_index "almacenamientos", ["servidor_id"], name: "index_almacenamientos_on_servidor_id"

  create_table "aplicaciones", force: true do |t|
    t.string   "nombre"
    t.string   "version"
    t.string   "referencia_externa"
    t.string   "descripcion"
    t.integer  "area_id"
    t.integer  "arquitectura_id"
    t.integer  "lenguaje_id"
    t.integer  "acceso_id"
    t.integer  "suministro_id"
    t.integer  "estado_aplicacion_id"
    t.integer  "tipo_aplicacion_id"
    t.string   "version_lenguaje"
    t.integer  "seguridad_aplicacion_id"
    t.integer  "mantenimiento_id"
    t.integer  "revisiones"
    t.integer  "usuarios_nominales"
    t.integer  "usuarios_concurrentes"
    t.integer  "criticidad_id"
    t.integer  "activo",                  default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "aplicaciones", ["acceso_id"], name: "index_aplicaciones_on_acceso_id"
  add_index "aplicaciones", ["area_id"], name: "index_aplicaciones_on_area_id"
  add_index "aplicaciones", ["arquitectura_id"], name: "index_aplicaciones_on_arquitectura_id"
  add_index "aplicaciones", ["criticidad_id"], name: "index_aplicaciones_on_criticidad_id"
  add_index "aplicaciones", ["estado_aplicacion_id"], name: "index_aplicaciones_on_estado_aplicacion_id"
  add_index "aplicaciones", ["lenguaje_id"], name: "index_aplicaciones_on_lenguaje_id"
  add_index "aplicaciones", ["mantenimiento_id"], name: "index_aplicaciones_on_mantenimiento_id"
  add_index "aplicaciones", ["seguridad_aplicacion_id"], name: "index_aplicaciones_on_seguridad_aplicacion_id"
  add_index "aplicaciones", ["suministro_id"], name: "index_aplicaciones_on_suministro_id"
  add_index "aplicaciones", ["tipo_aplicacion_id"], name: "index_aplicaciones_on_tipo_aplicacion_id"

  create_table "areas", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "arquitecturas", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bases_datos", force: true do |t|
    t.string   "nombre"
    t.string   "descripcion"
    t.integer  "area_id"
    t.integer  "estado_aplicacion_id"
    t.integer  "plataforma_bd_id"
    t.integer  "tipo_bd_id"
    t.string   "version_plataforma"
    t.integer  "seguridad_aplicacion_id"
    t.integer  "criticidad_id"
    t.integer  "suministro_id"
    t.integer  "mantenimiento_id"
    t.integer  "activo",                  default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bases_datos", ["area_id"], name: "index_bases_datos_on_area_id"
  add_index "bases_datos", ["criticidad_id"], name: "index_bases_datos_on_criticidad_id"
  add_index "bases_datos", ["estado_aplicacion_id"], name: "index_bases_datos_on_estado_aplicacion_id"
  add_index "bases_datos", ["mantenimiento_id"], name: "index_bases_datos_on_mantenimiento_id"
  add_index "bases_datos", ["plataforma_bd_id"], name: "index_bases_datos_on_plataforma_bd_id"
  add_index "bases_datos", ["seguridad_aplicacion_id"], name: "index_bases_datos_on_seguridad_aplicacion_id"
  add_index "bases_datos", ["suministro_id"], name: "index_bases_datos_on_suministro_id"
  add_index "bases_datos", ["tipo_bd_id"], name: "index_bases_datos_on_tipo_bd_id"

  create_table "contactos", force: true do |t|
    t.string   "nombre"
    t.string   "apellidos"
    t.string   "correo"
    t.string   "telefono"
    t.integer  "activo",     default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "criticidades", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "entornos", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "estado_aplicaciones", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "familias", force: true do |t|
    t.string   "denominacion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "activo",       default: 1
  end

  create_table "idiomas", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lenguajes", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mantenimientos", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plataforma_bds", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plataformas", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rol_servidores", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "seguridad_aplicaciones", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "servidores", force: true do |t|
    t.string   "hostname"
    t.string   "fqdn"
    t.integer  "tipo_up_id"
    t.integer  "plataforma_id"
    t.string   "virtual"
    t.integer  "familia_id"
    t.string   "sistema_operativo"
    t.integer  "idioma_id"
    t.integer  "tipo_licencia_id"
    t.integer  "numero_licencias"
    t.integer  "entorno_id"
    t.integer  "criticidad_id"
    t.boolean  "monitorizacion"
    t.integer  "rol_servidor_id"
    t.string   "descripcion"
    t.integer  "memoria"
    t.integer  "procesadores"
    t.string   "tipo_procesador"
    t.integer  "ubicacion_id"
    t.string   "rack"
    t.string   "marca"
    t.string   "modelo"
    t.string   "n_serie"
    t.integer  "tamanio"
    t.string   "conexiones"
    t.string   "tipo_conexiones"
    t.string   "intensidad"
    t.string   "potencia_maxima"
    t.integer  "activo",            default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "rol_secundario_id"
  end

  add_index "servidores", ["criticidad_id"], name: "index_servidores_on_criticidad_id"
  add_index "servidores", ["entorno_id"], name: "index_servidores_on_entorno_id"
  add_index "servidores", ["familia_id"], name: "index_servidores_on_familia_id"
  add_index "servidores", ["idioma_id"], name: "index_servidores_on_idioma_id"
  add_index "servidores", ["plataforma_id"], name: "index_servidores_on_plataforma_id"
  add_index "servidores", ["rol_servidor_id"], name: "index_servidores_on_rol_servidor_id"
  add_index "servidores", ["tipo_licencia_id"], name: "index_servidores_on_tipo_licencia_id"
  add_index "servidores", ["tipo_up_id"], name: "index_servidores_on_tipo_up_id"
  add_index "servidores", ["ubicacion_id"], name: "index_servidores_on_ubicacion_id"

  create_table "suministros", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipo_aplicaciones", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipo_bds", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipo_licencias", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipo_ups", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ubicaciones", force: true do |t|
    t.string   "denominacion"
    t.integer  "activo",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "usuarios", force: true do |t|
    t.string   "nombre"
    t.string   "apellidos"
    t.string   "correo"
    t.string   "password_digest"
    t.string   "usuario"
    t.integer  "activo",          default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "roles_mask",      default: 0
  end

end
