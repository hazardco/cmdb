class ConstructorGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)
  
  def variables
    fichero
    puts "ok"
    puts ARGV
  end
  
  def genera_modelo
    generate "model", ARGV.join(" ")
    
  end
  
  private
  def genera_controlador
    template "constructor_controller.rb", "app/controllers/#{@variables}_controller.rb"  
  end  
  
  def genera_vistas
    template "views/index.html.erb", "app/views/#{@variables}/index.html.erb"  
    template "views/_tabla.html.erb", "app/views/#{@variables}/_#{@variables}.html.erb"  
    template "views/_boton_atras.html.erb", "app/views/#{@variables}/_boton_atras.html.erb"  
    template "views/_boton_nuevo.html.erb", "app/views/#{@variables}/_boton_nuevo.html.erb"  
    template "views/_cabecera.html.erb", "app/views/#{@variables}/_cabecera.html.erb"  
    template "views/_cabecera_formulario.html.erb", "app/views/#{@variables}/_cabecera_formulario.html.erb"  
    template "views/_errores.html.erb", "app/views/#{@variables}/_errores.html.erb"  
    template "views/_form.html.erb", "app/views/#{@variables}/_form.html.erb"  
    template "views/edit.html.erb", "app/views/#{@variables}/_form.html.erb"  
    template "views/new.html.erb", "app/views/#{@variables}/_form.html.erb"  
  end
  
  def genera_test
    template "factories/constructor_factory.rb", "spec/factories/#{@variable}_factory.rb"  
    template "features/borrar_constructor_spec.rb", "spec/features/#{@variable}/borrar_#{@variable}_spec.rb"  
    template "features/crear_constructor_spec.rb", "spec/features/#{@variable}/crear_#{@variable}_spec.rb"  
    template "features/editar_constructor_spec.rb", "spec/features/#{@variable}/editar_#{@variable}_spec.rb"  
    template "features/ver_constructor_spec.rb", "spec/features/#{@variable}/ver_#{@variable}_spec.rb"   
  end
  
  
  private
  
  def fichero
    @constructor = file_name.camelize.pluralize
    @variable = file_name
    @variables = file_name.pluralize 
    @modelo = file_name.camelize
    
    file_name.underscore  
  end 

end
