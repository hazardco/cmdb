class <%= @constructor %>Controller < ApplicationController
  before_action :requiere_autenticacion!  
  before_filter :set_<%= @variable %>, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource :except => [:create]

  def index
    @<%= @variables %> = <%= @modelo %>.activo.page params[:page]
  end
  
  def new
    @<%= @variable %> = <%= @modelo %>.new
  end
  
  def edit
  end
  
  def create
    @<%= @variable %> = <%= @modelo %>.new(<%= @variable %>_params)
    if @area.save
      flash[:notice] = "<%= @modelo %> añadida correctamente"
      redirect_to <%= @variables %>_path
    else
      flash[:alert] = "Hubo un error al añadir el <%= @modelo %>"
      render "new"
    end
  end

  def update
    if @<%= @variable %>.update(<%= @variable %>_params)
      flash[:notice] = "<%= @modelo %> modificada correctamente"
      redirect_to <%= @variables %>_path
    else
      flash[:alert] = "Hubo un error al añadir el <%= @modelos %>"
      render "edit"
    end
  end
  
  def destroy
    @<%= @variable %>.activo = 0
    @<%= @variable %>.save
    flash[:notice] = "<%= @modelo %> borrada correctamente"
    redirect_to <%= @variables %>_path
  end
  
  private
  def <%= @variable %>_params
    params.require(:<%= @variable %>).permit(:denominacion)
  end
  
  def set_<%= @variable %>
    @area = <%= @modelo %>.activo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    flash[:alert] = "Vaya, parece que ha habido un error al buscar esa <%= @modelo %>"
    redirect_to areas_path
  end
end
