require 'spec_helper'

feature 'Borrar <%= @modelo %>' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    area = FactoryGirl.create(:<%= @variable %>)
    visit '/'
    click_link 'Desarrollo'
    click_link '<%= @constructor %>'
  end
  
  scenario 'Puedo borrar un <%= @modelo %>' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Borrar'
    expect(page).to have_content("<%= @modelo %> borrada correctamente")
  end

end