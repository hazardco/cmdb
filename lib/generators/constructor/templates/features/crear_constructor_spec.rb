require 'spec_helper'

feature 'Crear <%= @modelo %>' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link '<%= @constructor %>'
    click_link 'Nueva <%= @modelo %>'
  end
  
  scenario 'Puedo crear un <%= @modelo %>' do    
    fill_in 'Denominación', with: 'denominación genérica para TEST'
    click_button 'Crear <%= @modelo %>'
    
    expect(page).to have_content("<%= @modelo %> añadida correctamente")
  end
  
  scenario "No puedo crear un <%= @modelo %> sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear <%= @modelo %>'
    
    expect(page).to have_content("Hubo un error al añadir el <%= @modelo %>")
    
  end
end