require 'spec_helper'

feature 'Editar <%= @modelo %>' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:<%= @variable %>, denominacion: "denominación genérica para TEST")
    visit '/'
    click_link 'Desarrollo'
    click_link '<%= @constructor %>'
  end
  
  scenario 'Puedo editar un <%= @constructor %>' do
    expect(page).to have_content("denominación genérica para TEST")
    click_link 'Editar'
    fill_in "Denominación", with: 'nueva denominación genérica para TEST'
    click_button 'Actualizar <%= @modelo %>'
    expect(page).to have_content("nueva denominación genérica para TEST")
    expect(page).to have_content("<%= @modelo %> modificada correctamente")
  end
  
  scenario "No puedo actualizar un <%= @modelo %> sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar <%= @modelo %>"
    expect(page).to have_content("Hubo un error al añadir el <%= @modelo %>")
  end
end