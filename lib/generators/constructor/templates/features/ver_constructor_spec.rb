require 'spec_helper'

feature 'Ver <%= @constructor %>' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos las <%= @constructor %>' do
    jovenes = FactoryGirl.create(:area, denominacion: "denominación genérica para TEST")
    economia = FactoryGirl.create(:area, denominacion: "segundo TEST")
    visit '/'
    click_link 'Desarrollo'
    click_link '<%= @constructor %>'
    expect(page).to have_content("denominación genérica para TEST")
    expect(page).to have_content("segundo TEST")
  end
end