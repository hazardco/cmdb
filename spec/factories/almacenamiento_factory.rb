FactoryGirl.define do
  factory :almacenamiento do
    denominacion "dbmy01_0"
    fs 39
    tipo "disco"
    raid 5
    punto_montaje "/"
    observaciones "Fallos ocasionales"
  end
end