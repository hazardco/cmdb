FactoryGirl.define do
  factory :aplicacion do
    nombre "Cmdb"
    version "1"
    referencia_externa "cmdb.gobex.pri"
    descripcion "Base de datos de gestión de la configuración"
    area_id 1
    estado_aplicacion_id 1
    arquitectura_id 1
    tipo_aplicacion_id 1
    lenguaje_id 1 
    version_lenguaje "4"
    acceso_id 1
    seguridad_aplicacion_id 1
    suministro_id 1
    revisiones "1.2"
    usuarios_nominales 40
    usuarios_concurrentes 8
    criticidad_id 1
    activo 1
  end    
end