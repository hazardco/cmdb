FactoryGirl.define do
  factory :servidor do
    hostname "dbmy01"
    fqdn "dbmy01.eco.pri"
    tipo_up_id 1
    plataforma_id 1
    virtual "dbmy01vir"
    familia_id 1
    sistema_operativo "Linux"
    idioma_id 1
    tipo_licencia_id 1 
    numero_licencias "23"
    entorno_id 1
    criticidad_id 1
    monitorizacion true
    rol_servidor_id 1
    descripcion "Servidor de Rails"
    memoria 8
    procesadores 2
    tipo_procesador "x64"
    ubicacion_id 1
    rack "fvdfgf"
    marca "Dell"
    modelo "Opteron"
    n_serie "43534534GF34534"
    tamanio 1
    conexiones "2"
    tipo_conexiones "RJ45"
    intensidad "mucha"
    potencia_maxima "300"
    activo 1
  end    
end