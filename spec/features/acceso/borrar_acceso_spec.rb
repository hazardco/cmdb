require 'spec_helper'

feature 'Borrar Acceso' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:acceso)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Accesos'
  end
  
  scenario 'Puedo borrar un Acceso' do
    expect(page).to have_content("Público")
    click_link 'Borrar'
    expect(page).to have_content("Acceso borrado correctamente")
  end

end