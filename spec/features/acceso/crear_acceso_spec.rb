require 'spec_helper'

feature 'Crear Acceso' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Accesos'
    click_link 'Nuevo Acceso'
  end
  
  scenario 'Puedo crear un Acceso' do    
    fill_in 'Denominación', with: 'Público'
    click_button 'Crear Acceso'
    
    expect(page).to have_content("Acceso añadido correctamente")
  end
  
  scenario "No puedo crear un Acceso sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Acceso'
    
    expect(page).to have_content("Hubo un error al añadir el Acceso")
    
  end
end