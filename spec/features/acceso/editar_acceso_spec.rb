require 'spec_helper'

feature 'Editar Acceso' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:acceso, denominacion: "Público")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Accesos'
  end
  
  scenario 'Puedo editar un Acceso' do
    expect(page).to have_content("Público")
    click_link 'Editar'
    fill_in "Denominación", with: 'Privado'
    click_button 'Actualizar Acceso'
    expect(page).to have_content("Privado")
    expect(page).to have_content("Acceso modificado correctamente")
  end
  
  scenario "No puedo actualizar un Acceso sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Acceso"
    expect(page).to have_content("Hubo un error al añadir el Acceso")
  end
end