require 'spec_helper'

feature 'Ver Accesos' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos las Accesos' do
    FactoryGirl.create(:acceso, denominacion: "Público")
    FactoryGirl.create(:acceso, denominacion: "Privado")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Accesos'
    expect(page).to have_content("Público")
    expect(page).to have_content("Privado")
  end
end