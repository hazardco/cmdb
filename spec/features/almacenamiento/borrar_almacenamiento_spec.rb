require 'spec_helper'

feature 'Borrar Almacenamiento' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:tipo_up)
    FactoryGirl.create(:plataforma)
    FactoryGirl.create(:familia)
    FactoryGirl.create(:idioma)
    FactoryGirl.create(:tipo_licencia)
    FactoryGirl.create(:entorno)
    FactoryGirl.create(:criticidad)
    FactoryGirl.create(:rol_servidor)
    FactoryGirl.create(:ubicacion)
    FactoryGirl.create(:servidor)
    
    area = FactoryGirl.create(:almacenamiento)
    visit '/'
    click_link 'Sistemas'
    click_link 'Servidores'
    click_link 'Ver'
    click_link 'Almacenamientos'

  end
  
  scenario 'Puedo borrar un Almacenamiento' do
    expect(page).to have_content("dbmy01_0")
    click_link 'Borrar'
    expect(page).to have_content("Almacenamiento borrado correctamente")
  end

end