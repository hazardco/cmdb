require 'spec_helper'

feature 'Crear Almacenamiento' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    
    FactoryGirl.create(:tipo_up)
    FactoryGirl.create(:plataforma)
    FactoryGirl.create(:familia)
    FactoryGirl.create(:idioma)
    FactoryGirl.create(:tipo_licencia)
    FactoryGirl.create(:entorno)
    FactoryGirl.create(:criticidad)
    FactoryGirl.create(:rol_servidor)
    FactoryGirl.create(:ubicacion)
    FactoryGirl.create(:servidor)
    
    click_link 'Sistemas'
    click_link 'Servidores'
    click_link 'Ver'
    click_link 'Almacenamientos'
    click_link 'Nuevo Almacenamiento'
    
  end
  
  scenario 'Puedo crear un almacenamiento' do    
    fill_in 'Denominación', with: 'dbmy01_0'
    fill_in 'FS', with: 'FAT32'
    fill_in "Tipo", with: ""
    fill_in "Raid", with: "5"
    fill_in "Punto de Montaje", with: "/"
    fill_in "Observaciones", with: ""
    click_button 'Crear Almacenamiento'
    
    expect(page).to have_content("Almacenamiento añadido correctamente")
  end
  
  scenario "No puedo crear un Almacenamiento sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Almacenamiento'
    
    expect(page).to have_content("Hubo un error al añadir el Almacenamiento")
    
  end
end