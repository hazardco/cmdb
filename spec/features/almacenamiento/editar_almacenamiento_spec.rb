require 'spec_helper'

feature 'Editar Almacenamiento' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:tipo_up)
    FactoryGirl.create(:plataforma)
    FactoryGirl.create(:familia)
    FactoryGirl.create(:idioma)
    FactoryGirl.create(:tipo_licencia)
    FactoryGirl.create(:entorno)
    FactoryGirl.create(:criticidad)
    FactoryGirl.create(:rol_servidor)
    FactoryGirl.create(:ubicacion)
    FactoryGirl.create(:servidor)
    
    FactoryGirl.create(:almacenamiento)

    visit '/'
    click_link 'Sistemas'
    click_link 'Servidores'
  end
  
  scenario 'Puedo editar un Almacenamiento' do
    click_link 'Ver'
    expect(page).to have_content("dbmy01.eco.pri")
    click_link 'Almacenamientos' 
    expect(page).to have_content("dbmy01_0")    
    click_link 'Editar'
    fill_in "Denominación", with: 'dbmy03_3'
    click_button 'Actualizar Almacenamiento'
    expect(page).to have_content("dbmy03_3")
    expect(page).to have_content("Almacenamiento modificado correctamente")
  end
  
  scenario "No puedo actualizar un Almacenamiento sin denominación" do
    click_link 'Ver'
    expect(page).to have_content("dbmy01.eco.pri")
    click_link 'Almacenamientos' 
    expect(page).to have_content("dbmy01_0")    
    click_link 'Editar'
    fill_in "Denominación", with: ''
    click_button 'Actualizar Almacenamiento'
    expect(page).to have_content("Hubo un error al modificar el Almacenamiento")
  end
end