require 'spec_helper'
feature 'Ver Áreas' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:tipo_up)
    FactoryGirl.create(:plataforma)
    FactoryGirl.create(:familia)
    FactoryGirl.create(:idioma)
    FactoryGirl.create(:tipo_licencia)
    FactoryGirl.create(:entorno)
    FactoryGirl.create(:criticidad)
    FactoryGirl.create(:rol_servidor)
    FactoryGirl.create(:ubicacion)
    FactoryGirl.create(:servidor)
    
    FactoryGirl.create(:almacenamiento)
    visit '/'
    click_link 'Sistemas'
    click_link 'Servidores'    
    
  end
      
  scenario 'Puedo ver todos los Almacenamientos' do
    click_link 'Ver'
    expect(page).to have_content("dbmy01.eco.pri")
    click_link 'Almacenamientos' 
    expect(page).to have_content("dbmy01_0")
  end
end