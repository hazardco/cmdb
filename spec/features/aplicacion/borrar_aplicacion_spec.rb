require 'spec_helper'

feature 'Borrar Aplicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:area)
    FactoryGirl.create(:estado_aplicacion)
    FactoryGirl.create(:arquitectura)
    FactoryGirl.create(:tipo_aplicacion)
    FactoryGirl.create(:lenguaje)
    FactoryGirl.create(:acceso)
    FactoryGirl.create(:seguridad_aplicacion)
    FactoryGirl.create(:suministro)
    FactoryGirl.create(:mantenimiento)
    FactoryGirl.create(:criticidad)
    
    FactoryGirl.create(:aplicacion)
    
    visit '/'
    click_link 'Desarrollo'
    click_link 'Aplicaciones'
  end
  
  scenario 'Puedo borrar un Servidor' do
    expect(page).to have_content("Cmdb")
    click_link 'Borrar'
    expect(page).to have_content("Aplicación borrada correctamente")
  end

end