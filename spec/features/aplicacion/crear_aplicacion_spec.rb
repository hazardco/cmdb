require "spec_helper"

feature "Crear Aplicación" do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:area)
    FactoryGirl.create(:estado_aplicacion)
    FactoryGirl.create(:arquitectura)
    FactoryGirl.create(:tipo_aplicacion)
    FactoryGirl.create(:lenguaje)
    FactoryGirl.create(:acceso)
    FactoryGirl.create(:seguridad_aplicacion)
    FactoryGirl.create(:suministro)
    FactoryGirl.create(:mantenimiento)
    FactoryGirl.create(:criticidad)
    
    visit '/'
    click_link 'Desarrollo'
    click_link 'Aplicaciones'
    click_link 'Nueva Aplicación'
  end
  scenario "Puedo Crear un Aplicación" do
    fill_in 'Nombre', with: "Gedindep"
    fill_in 'Versión', with: "3.1"
    fill_in "Referencia Externa", with: "dinamizadores.gobex.es"
    fill_in "Descripción", with: "Gestión de las actividades de los Dinamizadores Deportivos"
    fill_in "Versión del Lenguaje", with: "3"
    fill_in "Revisiones", with: "4"
    fill_in "Usuarios Nominales", with: "100"
    fill_in "Usuarios Concurrentes", with: "10"
    
    click_button 'Crear Aplicación'
    expect(page).to have_content("Aplicación añadida correctamente")
  end
  
  scenario "No puedo Crear una Aplicación sin nombre" do
    fill_in 'Nombre', with: ""
    fill_in 'Versión', with: "3.1"
    fill_in "Referencia Externa", with: "dinamizadores.gobex.es"
    fill_in "Descripción", with: "Gestión de las actividades de los Dinamizadores Deportivos"
    fill_in "Versión del Lenguaje", with: "3"
    fill_in "Revisiones", with: "4"
    fill_in "Usuarios Nominales", with: "100"
    fill_in "Usuarios Concurrentes", with: "10"
    
    click_button 'Crear Aplicación'
    expect(page).to have_content("Hubo un error al añadir la Aplicación")
  end
end