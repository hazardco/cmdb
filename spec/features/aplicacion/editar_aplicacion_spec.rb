require 'spec_helper'

feature 'Editar Aplicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:area)
    FactoryGirl.create(:estado_aplicacion)
    FactoryGirl.create(:arquitectura)
    FactoryGirl.create(:tipo_aplicacion)
    FactoryGirl.create(:lenguaje)
    FactoryGirl.create(:acceso)
    FactoryGirl.create(:seguridad_aplicacion)
    FactoryGirl.create(:suministro)
    FactoryGirl.create(:mantenimiento)
    FactoryGirl.create(:criticidad)
    
    FactoryGirl.create(:aplicacion)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Aplicaciones'
  end
  
  scenario 'Puedo editar una Aplicación' do
    expect(page).to have_content("Cmdb")
    click_link 'Editar'
    fill_in "Nombre", with: 'GeDinDep'
    click_button 'Actualizar Aplicación'
    expect(page).to have_content("GeDinDep")
    expect(page).to have_content("Aplicación modificada correctamente")
  end
  
  scenario "No puedo actualizar una Aplicación sin nombre" do
    click_link "Editar"
    fill_in "Nombre", with: ""
    click_button "Actualizar Aplicación"
    expect(page).to have_content("Hubo un error al añadir la Aplicación")
  end
end