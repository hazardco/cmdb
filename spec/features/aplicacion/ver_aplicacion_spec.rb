require 'spec_helper'

feature 'Ver Aplicaciones' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
  scenario 'Puedo ver todas las Aplicaciones' do
    FactoryGirl.create(:area)
    FactoryGirl.create(:estado_aplicacion)
    FactoryGirl.create(:arquitectura)
    FactoryGirl.create(:tipo_aplicacion)
    FactoryGirl.create(:lenguaje)
    FactoryGirl.create(:acceso)
    FactoryGirl.create(:seguridad_aplicacion)
    FactoryGirl.create(:suministro)
    FactoryGirl.create(:mantenimiento)
    FactoryGirl.create(:criticidad)
    
    FactoryGirl.create(:aplicacion)
    
    visit '/'
    click_link 'Desarrollo'
    click_link 'Aplicaciones'
    expect(page).to have_content("Cmdb")
    expect(page).to have_content("Ruby")
    expect(page).to have_content("Extrema")
  end
end