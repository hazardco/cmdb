require 'spec_helper'

feature 'Borrar Área' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    area = FactoryGirl.create(:area)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Áreas'
  end
  
  scenario 'Puedo borrar un Área' do
    expect(page).to have_content("Jóvenes y Deportes")
    click_link 'Borrar'
    expect(page).to have_content("Área borrada correctamente")
  end

end