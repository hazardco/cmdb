require 'spec_helper'

feature 'Crear Área' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Áreas'
    click_link 'Nueva Área'
  end
  
  scenario 'Puedo crear un área' do    
    fill_in 'Denominación', with: 'Jóvenes y Desarrollo'
    click_button 'Crear Área'
    
    expect(page).to have_content("Área añadida correctamente")
  end
  
  scenario "No puedo crear un Área sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Área'
    
    expect(page).to have_content("Hubo un error al añadir el Área")
    
  end
end