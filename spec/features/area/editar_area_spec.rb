require 'spec_helper'

feature 'Editar Área' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    windows = FactoryGirl.create(:area, denominacion: "Jóvenes y Deportes")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Áreas'
  end
  
  scenario 'Puedo editar un Área' do
    expect(page).to have_content("Jóvenes y Deportes")
    click_link 'Editar'
    fill_in "Denominación", with: 'Economía'
    click_button 'Actualizar Área'
    expect(page).to have_content("Economía")
    expect(page).to have_content("Área modificada correctamente")
  end
  
  scenario "No puedo actualizar un Área sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Área"
    expect(page).to have_content("Hubo un error al añadir el Área")
  end
end