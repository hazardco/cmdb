require 'spec_helper'

feature 'Ver Áreas' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos las Áreas' do
    jovenes = FactoryGirl.create(:area, denominacion: "Jóvenes y Deportes")
    economia = FactoryGirl.create(:area, denominacion: "Economía")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Áreas'
    expect(page).to have_content("Jóvenes y Deportes")
    expect(page).to have_content("Economía")
  end
end