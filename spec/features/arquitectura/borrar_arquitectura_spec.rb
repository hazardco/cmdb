require 'spec_helper'

feature 'Borrar Arquitectura' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    arquitectura = FactoryGirl.create(:arquitectura)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Arquitectura'
  end
  
  scenario 'Puedo borrar una Arquitectura' do
    expect(page).to have_content("MVC")
    click_link 'Borrar'
    expect(page).to have_content("Arquitectura borrada correctamente")
  end

end