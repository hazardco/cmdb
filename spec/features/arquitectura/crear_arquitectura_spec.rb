require 'spec_helper'

feature 'Crear Arquitectura' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Arquitectura'
    click_link 'Nueva Arquitectura'
  end
  
  scenario 'Puedo crear una Arquitectura' do    
    fill_in 'Denominación', with: 'MVC'
    click_button 'Crear Arquitectura'
    
    expect(page).to have_content("Arquitectura añadida correctamente")
  end
  
  scenario "No puedo crear una Arquitectura sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Arquitectura'
    
    expect(page).to have_content("Hubo un error al añadir la Arquitectura")
    
  end
end