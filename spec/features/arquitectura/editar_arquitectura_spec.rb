require 'spec_helper'

feature 'Editar Arquitectura' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    windows = FactoryGirl.create(:arquitectura, denominacion: "MVC")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Arquitectura'
  end
  
  scenario 'Puedo editar una Arquitectura' do
    expect(page).to have_content("MVC")
    click_link 'Editar'
    fill_in "Denominación", with: 'Escritorio'
    click_button 'Actualizar Arquitectura'
    expect(page).to have_content("Escritorio")
    expect(page).to have_content("Arquitectura modificada correctamente")
  end
  
  scenario "No puedo actualizar una Arquitectura sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Arquitectura"
    expect(page).to have_content("Hubo un error al añadir la Arquitectura")
  end
end