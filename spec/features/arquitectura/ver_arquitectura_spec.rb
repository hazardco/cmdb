require 'spec_helper'

feature 'Ver Arquitecturas' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos las Arquitecturas' do
    FactoryGirl.create(:arquitectura, denominacion: "MVC")
    FactoryGirl.create(:arquitectura, denominacion: "Escritorio")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Arquitectura'
    expect(page).to have_content("MVC")
    expect(page).to have_content("Escritorio")
  end
end