require 'spec_helper'

feature 'Añadir un Servidor' do
  scenario 'Se puede crear un Servidor' do
    visit '/'
    click_link 'Servidores'
    click_link 'Nuevo Servidor'
    fill_in 'hostname', with: 'dbmy01'
    fill_in 'FQDN', with: 'dbmy.eco.pri'
    click_button 'Crear Servidor'
  
    expect(page).to have_content("Servidor añadido correctamente")
  end
end