require 'spec_helper'

feature 'Borrar Criticidad' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)

    criticidad = FactoryGirl.create(:criticidad, denominacion: "Extrema")
    visit '/'
    click_link 'Sistemas'
    click_link 'Criticidades'
  end
  
  scenario 'Puedo borrar un Criticidad' do
    expect(page).to have_content("Extrema")
    click_link 'Borrar'
    expect(page).to have_content("Criticidad borrada correctamente")
  end

end