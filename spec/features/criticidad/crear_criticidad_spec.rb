require 'spec_helper'

feature 'Crear Criticidad' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Sistemas'
    click_link 'Criticidades'
    click_link 'Nueva Criticidad'
  end
  
  scenario 'Puedo crear una criticidad' do
    fill_in 'Denominación', with: 'Extrema'
    click_button 'Crear Criticidad'
    
    expect(page).to have_content("Criticidad añadida correctamente")
  end
  
  scenario "No puedo crear un Entorno sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Criticidad'
    
    expect(page).to have_content("Hubo un error al añadir la Criticidad")
    
  end
end