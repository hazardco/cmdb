require 'spec_helper'

feature 'Editar Criticidad' do
  
  before do
    extrema = FactoryGirl.create(:criticidad, denominacion: "Extrema")
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Sistemas'
    click_link 'Criticidades'
  end
  
  scenario 'Puedo editar una Criticidad' do
    expect(page).to have_content("Extrema")
    click_link 'Editar'
    fill_in "Denominación", with: 'Leve'
    click_button 'Actualizar Criticidad'
    expect(page).to have_content("Leve")
    expect(page).to have_content("Criticidad modificada correctamente")
  end
  
  scenario "No puedo actualizar una Criticidad sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Criticidad"
    expect(page).to have_content("Hubo un error al añadir la Criticidad")
  end
end