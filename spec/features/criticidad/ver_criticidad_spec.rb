require 'spec_helper'

feature 'Ver Criticidad' do
  
  before do
    
  end
  
  scenario 'Puedo ver todas las Criticidades' do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    windows = FactoryGirl.create(:criticidad, denominacion: "Leve")
    mac = FactoryGirl.create(:criticidad, denominacion: "Media")
    linux = FactoryGirl.create(:criticidad, denominacion: "Extrema")
    visit '/'
    click_link 'Sistemas'
    click_link 'Criticidades'
    expect(page).to have_content("Leve")
    expect(page).to have_content("Media")
    expect(page).to have_content("Extrema")
  end
end