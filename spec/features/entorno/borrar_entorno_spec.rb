require 'spec_helper'

feature 'Borrar Entorno' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    desarrollo = FactoryGirl.create(:entorno, denominacion: "Desarrollo")
    visit '/'
    click_link 'Sistemas'
    click_link 'Entornos'
  end
  
  scenario 'Puedo borrar un Entorno' do
    expect(page).to have_content("Desarrollo")
    click_link 'Borrar'
    expect(page).to have_content("Entorno borrado correctamente")
  end

end