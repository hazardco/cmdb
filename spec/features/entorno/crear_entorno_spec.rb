require 'spec_helper'

feature 'Crear Entorno' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Sistemas'
    click_link 'Entornos'
    click_link 'Nuevo Entorno'
  end
  
  scenario 'Puedo crear un entorno' do    
    fill_in 'Denominación', with: 'Desarrollo'
    click_button 'Crear Entorno'
    
    expect(page).to have_content("Entorno añadido correctamente")
  end
  
  scenario "No puedo crear un Entorno sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Entorno'
    
    expect(page).to have_content("Hubo un error al añadir el Entorno")
    
  end
end