require 'spec_helper'

feature 'Editar Entorno' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    windows = FactoryGirl.create(:entorno, denominacion: "Desarrollo")
    visit '/'
    click_link 'Sistemas'
    click_link 'Entornos'
  end
  
  scenario 'Puedo editar un Entorno' do
    expect(page).to have_content("Desarrollo")
    click_link 'Editar'
    fill_in "Denominación", with: 'Pruebas'
    click_button 'Actualizar Entorno'
    expect(page).to have_content("Pruebas")
    expect(page).to have_content("Entorno modificado correctamente")
  end
  
  scenario "No puedo actualizar un Entorno sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Entorno"
    expect(page).to have_content("Hubo un error al añadir el Entorno")
  end
end