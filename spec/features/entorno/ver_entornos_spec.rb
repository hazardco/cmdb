require 'spec_helper'

feature 'Ver Entornos' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos los Entornos' do
    windows = FactoryGirl.create(:entorno, denominacion: "Desarrollo")
    mac = FactoryGirl.create(:entorno, denominacion: "Preproducción")
    linux = FactoryGirl.create(:entorno, denominacion: "Producción")
    visit '/'
    click_link 'Sistemas'
    click_link 'Entornos'
    expect(page).to have_content("Desarrollo")
    expect(page).to have_content("Preproducción")
    expect(page).to have_content("Producción")
  end
end