require 'spec_helper'

feature 'Borrar Estado de Aplicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:estado_aplicacion)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Estados de Aplicación'
  end
  
  scenario 'Puedo borrar un Estado de Aplicación' do
    expect(page).to have_content("Desarrollo")
    click_link 'Borrar'
    expect(page).to have_content("Estado de la Aplicación borrado correctamente")
  end
end