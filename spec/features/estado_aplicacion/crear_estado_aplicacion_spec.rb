require 'spec_helper'

feature 'Crear Estado de Aplicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Estados de Aplicación'
    click_link 'Nuevo Estado de Aplicación'
  end
  
  scenario 'Puedo crear un Estado de Aplicación' do    
    fill_in 'Denominación', with: 'Desarrollo'
    click_button 'Crear Estado de Aplicación'
    
    expect(page).to have_content("Estado de la Aplicación añadido correctamente")
  end
  
  scenario "No puedo crear un Estado de Aplicación sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Estado de Aplicación'
    
    expect(page).to have_content("Hubo un error al añadir el Estado de la Aplicación")
    
  end
end