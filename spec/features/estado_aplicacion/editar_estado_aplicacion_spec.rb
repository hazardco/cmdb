require 'spec_helper'

feature 'Editar Estado de Aplicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:estado_aplicacion, denominacion: "Desarrollo")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Estados de Aplicación'
  end
  
  scenario 'Puedo editar un Estado de Aplicación' do
    expect(page).to have_content("Desarrollo")
    click_link 'Editar'
    fill_in "Denominación", with: 'Producción'
    click_button 'Actualizar Estado de Aplicación'
    expect(page).to have_content("Producción")
    expect(page).to have_content("Estado de la Aplicación modificado correctamente")
  end
  
  scenario "No puedo actualizar un Estado de Aplicación sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Estado de Aplicación"
    expect(page).to have_content("Hubo un error al añadir el Estado de la Aplicación")
  end
end