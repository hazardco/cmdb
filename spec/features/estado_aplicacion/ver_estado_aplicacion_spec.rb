require 'spec_helper'

feature 'Ver Estados de Aplicación' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos los Estados de Aplicación' do
    FactoryGirl.create(:estado_aplicacion, denominacion: "Desarrollo")
    FactoryGirl.create(:estado_aplicacion, denominacion: "Producción")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Estados de Aplicación'
    expect(page).to have_content("Desarrollo")
    expect(page).to have_content("Producción")
  end
end