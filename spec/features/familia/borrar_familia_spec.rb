require 'spec_helper'

feature 'Borrar Familia de SO' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    windows = FactoryGirl.create(:familia, denominacion: "Windows")
    visit '/'
    click_link 'Sistemas'
    click_link 'SO Familias'
  end
  
  scenario 'Puedo borrar una Familias de SO' do
    expect(page).to have_content("Windows")
    click_link 'Borrar'
    expect(page).to have_content("Familia de SO borrada correctamente")
  end

end