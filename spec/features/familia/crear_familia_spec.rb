require 'spec_helper'

feature 'Crear Familia de Sistema Operativo' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Sistemas'
    click_link 'SO Familias'
    click_link 'Nueva Familia'
  end
  scenario 'Puedo crear una familia de SO nueva' do
    fill_in 'Denominación', with: 'Windows'
    click_button 'Crear Familia'
    
    expect(page).to have_content("Familia de SO añadida correctamente")
  end
  
  scenario "No puedo crear una familia de SO sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Familia'
    
    expect(page).to have_content("Hubo un error al añadir la Familia de SO")
    
  end
end