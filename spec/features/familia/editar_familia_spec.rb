require 'spec_helper'

feature 'Editar Familia de SO' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    windows = FactoryGirl.create(:familia, denominacion: "Güindous")
    visit '/'
    click_link 'Sistemas'
    click_link 'SO Familias'
  end
  
  scenario 'Puedo editar una Familias de SO' do
    expect(page).to have_content("Güindous")
    click_link 'Editar'
    fill_in "Denominación", with: 'Windows'
    click_button 'Actualizar Familia'
    expect(page).to have_content("Windows")
    expect(page).to have_content("Familia de SO modificada correctamente")
  end
  
  scenario "No puedo actualizar una Familia de SO sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Familia"
    expect(page).to have_content("Hubo un error al añadir la Familia de SO")
  end
end