require 'spec_helper'

feature 'Ver Familias de SO' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
  scenario 'Puedo ver todas las Familias de SO' do
    windows = FactoryGirl.create(:familia, denominacion: "Windows")
    mac = FactoryGirl.create(:familia, denominacion: "Mac")
    linux = FactoryGirl.create(:familia, denominacion: "Linux")
    visit '/'
    click_link 'Sistemas'
    click_link 'SO Familias'
    expect(page).to have_content("Windows")
    expect(page).to have_content("Mac")
    expect(page).to have_content("Linux")
  end
end