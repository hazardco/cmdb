require 'spec_helper'

feature 'Borrar Idioma' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    windows = FactoryGirl.create(:idioma, denominacion: "Inglés")
    visit '/'
    click_link 'Sistemas'
    click_link 'Idiomas'
  end
  
  scenario 'Puedo borrar un Idioma' do
    expect(page).to have_content("Inglés")
    click_link 'Borrar'
    expect(page).to have_content("Idioma borrado correctamente")
  end

end