require 'spec_helper'

feature 'Crear Idioma' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Sistemas'
    click_link 'Idiomas'
    click_link 'Nuevo Idioma'
  end
  scenario 'Puedo crear un idioma' do   
    fill_in 'Denominación', with: 'Español'
    click_button 'Crear Idioma'
    
    expect(page).to have_content("Idioma añadido correctamente")
  end
  
  scenario "No puedo crear un Idioma sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Idioma'
    
    expect(page).to have_content("Hubo un error al añadir el Idioma")
    
  end
end