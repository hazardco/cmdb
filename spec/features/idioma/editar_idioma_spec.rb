require 'spec_helper'

feature 'Editar Idioma' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    windows = FactoryGirl.create(:idioma, denominacion: "Inglés")
    visit '/'
    click_link 'Sistemas'
    click_link 'Idiomas'
  end
  
  scenario 'Puedo editar un Entorno' do
    expect(page).to have_content("Inglés")
    click_link 'Editar'
    fill_in "Denominación", with: 'Español'
    click_button 'Actualizar Idioma'
    expect(page).to have_content("Español")
    expect(page).to have_content("Idioma modificado correctamente")
  end
  
  scenario "No puedo actualizar un Idioma sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Idioma"
    expect(page).to have_content("Hubo un error al añadir el Idioma")
  end
end