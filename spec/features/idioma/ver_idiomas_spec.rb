require 'spec_helper'

feature 'Ver Idiomas' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
  scenario 'Puedo ver todos los Idiomas' do
    windows = FactoryGirl.create(:idioma, denominacion: "Español")
    mac = FactoryGirl.create(:idioma, denominacion: "Inglés")
    linux = FactoryGirl.create(:idioma, denominacion: "Alemán")
    visit '/'
    click_link 'Sistemas'
    click_link 'Idiomas'
    expect(page).to have_content("Español")
    expect(page).to have_content("Inglés")
    expect(page).to have_content("Alemán")
  end
end