require 'spec_helper'

feature 'Borrar lenguaje' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    lenguaje = FactoryGirl.create(:lenguaje)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Lenguajes'
  end
  
  scenario 'Puedo borrar un lenguaje' do
    expect(page).to have_content("Ruby")
    click_link 'Borrar'
    expect(page).to have_content("Lenguaje borrado correctamente")
  end
end