require 'spec_helper'

feature 'Crear lenguaje' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Lenguajes'
    click_link 'Nuevo Lenguaje'
  end
  
  scenario 'Puedo crear un Lenguaje' do    
    fill_in 'Denominación', with: 'Ruby'
    click_button 'Crear Lenguaje'
    
    expect(page).to have_content("Lenguaje añadido correctamente")
  end
  
  scenario "No puedo crear un Lenguaje sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Lenguaje'
    
    expect(page).to have_content("Hubo un error al añadir el Lenguaje")
    
  end
end