require 'spec_helper'

feature 'Editar Lenguaje' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    ruby = FactoryGirl.create(:lenguaje, denominacion: "Ruby")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Lenguajes'
  end
  
  scenario 'Puedo editar un Lenguaje' do
    expect(page).to have_content("Ruby")
    click_link 'Editar'
    fill_in "Denominación", with: 'PHP'
    click_button 'Actualizar Lenguaje'
    expect(page).to have_content("PHP")
    expect(page).to have_content("Lenguaje modificado correctamente")
  end
  
  scenario "No puedo actualizar un Lenguaje sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Lenguaje"
    expect(page).to have_content("Hubo un error al añadir el Lenguaje")
  end
end