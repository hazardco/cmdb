require 'spec_helper'

feature 'Ver Lenguajes' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos las Lenguajes' do
    FactoryGirl.create(:lenguaje, denominacion: "Ruby")
    FactoryGirl.create(:lenguaje, denominacion: "PHP")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Lenguajes'
    expect(page).to have_content("Ruby")
    expect(page).to have_content("PHP")
  end
end