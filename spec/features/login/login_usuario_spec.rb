require 'spec_helper'

feature 'Login de Usuario' do
  before do
    usuario = FactoryGirl.create(:usuario)
  end
  scenario 'Puedo entrar en la aplicación con un Usuario válido' do
    visit '/'
    fill_in "Usuario", with: "luismiguel.cabezas"
    fill_in "Password", with: "lmcabezas"
    click_button "Entrar"
    expect(page).to have_content("Login correcto")
  end
  scenario 'No puedo entrar en la aplicación con un Usuario falso' do
    visit '/'
    fill_in "Usuario", with: "luismiguel.cabezas"
    fill_in "Password", with: "mal"
    click_button "Entrar"
    expect(page).to have_content("Login incorrecto")
  end
  scenario 'Puedo salir de la aplicación haciendo logout' do
    visit '/'
    fill_in "Usuario", with: "luismiguel.cabezas"
    fill_in "Password", with: "lmcabezas"
    click_button "Entrar"
    expect(page).to have_content("Login correcto")
    click_link "Luis Miguel Cabezas"
    click_link "Salir"
    expect(page).to have_content("Ha salido correctamente de la aplicación ")
  end
end