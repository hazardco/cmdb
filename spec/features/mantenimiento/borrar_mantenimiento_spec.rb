require 'spec_helper'

feature 'Borrar Mantenimiento' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:mantenimiento)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Mantenimientos'
  end
  
  scenario 'Puedo borrar un Mantenimiento' do
    expect(page).to have_content("Interno")
    click_link 'Borrar'
    expect(page).to have_content("Mantenimiento borrado correctamente")
  end
end