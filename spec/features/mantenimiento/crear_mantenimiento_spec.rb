require 'spec_helper'

feature 'Crear Mantenimiento' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Mantenimientos'
    click_link 'Nuevo Mantenimiento'
  end
  
  scenario 'Puedo crear un Mantenimiento' do    
    fill_in 'Denominación', with: 'Interno'
    click_button 'Crear Mantenimiento'
    
    expect(page).to have_content("Mantenimiento añadido correctamente")
  end
  
  scenario "No puedo crear un Suministro sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Mantenimiento'
    
    expect(page).to have_content("Hubo un error al añadir el Mantenimiento")
    
  end
end