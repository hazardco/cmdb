require 'spec_helper'

feature 'Editar Mantenimiento' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:mantenimiento, denominacion: "Interno")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Mantenimientos'
  end
  
  scenario 'Puedo editar un Mantenimiento' do
    expect(page).to have_content("Interno")
    click_link 'Editar'
    fill_in "Denominación", with: 'Externo'
    click_button 'Actualizar Mantenimiento'
    expect(page).to have_content("Externo")
    expect(page).to have_content("Mantenimiento modificado correctamente")
  end
  
  scenario "No puedo actualizar un Mantenimiento sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Mantenimiento"
    expect(page).to have_content("Hubo un error al añadir el Mantenimiento")
  end
end