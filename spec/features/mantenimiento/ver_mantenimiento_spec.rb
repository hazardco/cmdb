require 'spec_helper'

feature 'Ver Mantenimientos' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos las Mantenimientos' do
    FactoryGirl.create(:mantenimiento, denominacion: "Interno")
    FactoryGirl.create(:mantenimiento, denominacion: "Externo")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Mantenimientos'
    expect(page).to have_content("Interno")
    expect(page).to have_content("Externo")
  end
end