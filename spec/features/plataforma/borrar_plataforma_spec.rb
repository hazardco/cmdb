require 'spec_helper'

feature 'Borrar Plataforma' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    windows = FactoryGirl.create(:plataforma, denominacion: "SLOT2DELL")
    visit '/'
    click_link 'Sistemas'
    click_link 'Plataformas'
  end
  
  scenario 'Puedo borrar una Plataforma' do
    expect(page).to have_content("SLOT2DELL")
    click_link 'Borrar'
    expect(page).to have_content("Plataforma borrada correctamente")
  end

end