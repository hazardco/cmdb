require 'spec_helper'

feature 'Crear Plataforma' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Sistemas'
    click_link 'Plataformas'
    click_link 'Nueva Plataforma'
  end
  scenario 'Puedo crear una Plataforma' do 
    fill_in 'Denominación', with: 'Español'
    click_button 'Crear Plataforma'
    
    expect(page).to have_content("Plataforma añadida correctamente")
  end
  
  scenario "No puedo crear una Plataforma sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Plataforma'
    
    expect(page).to have_content("Hubo un error al añadir la Plataforma")
  end
end