require 'spec_helper'

feature 'Editar Plataforma' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    windows = FactoryGirl.create(:plataforma, denominacion: "SLOT2DELL")
    visit '/'
    click_link 'Sistemas'
    click_link 'Plataformas'
  end
  
  scenario 'Puedo editar una Plataforma' do
    expect(page).to have_content("SLOT2DELL")
    click_link 'Editar'
    fill_in "Denominación", with: 'WMWare'
    click_button 'Actualizar Plataforma'
    expect(page).to have_content("WMWare")
    expect(page).to have_content("Plataforma modificada correctamente")
  end
  
  scenario "No puedo actualizar una Plataforma sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Plataforma"
    expect(page).to have_content("Hubo un error al añadir la Plataforma")
  end
end