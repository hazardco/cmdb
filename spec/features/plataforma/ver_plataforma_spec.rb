require 'spec_helper'

feature 'Ver Plataformas' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
  scenario 'Puedo ver todos los Idiomas' do
    slot2dell = FactoryGirl.create(:plataforma, denominacion: "SLOT2DELL")
    wmware = FactoryGirl.create(:plataforma, denominacion: "WMWare")
    citrix = FactoryGirl.create(:plataforma, denominacion: "Citrix")
    visit '/'
    click_link 'Sistemas'
    click_link 'Plataformas'
    expect(page).to have_content("SLOT2DELL")
    expect(page).to have_content("WMWare")
    expect(page).to have_content("Citrix")
  end
end