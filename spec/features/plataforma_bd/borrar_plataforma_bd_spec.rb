require 'spec_helper'

feature 'Borrar Plataforma de Base de Datos' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:plataforma_bd)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Plataforma BD'
  end
  
  scenario 'Puedo borrar una Plataforma de Base de Datos' do
    expect(page).to have_content("MySQL")
    click_link 'Borrar'
    expect(page).to have_content("Plataforma de Base de Datos borrada correctamente")
  end
end