require 'spec_helper'

feature 'Crear Plataforma de Base de Datos' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Plataforma BD'
    click_link 'Nueva Plataforma de Base de Datos'
  end
  
  scenario 'Puedo crear una Plataforma de Base de Datos' do    
    fill_in 'Denominación', with: 'MySQL'
    click_button 'Crear Plataforma de Base de Datos'
    
    expect(page).to have_content("Plataforma de Base de Datos añadida correctamente")
  end
  
  scenario "No puedo crear una Plataforma de Base de Datos sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Plataforma de Base de Datos'
    
    expect(page).to have_content("Hubo un error al añadir la Plataforma de Base de Datos")
    
  end
end