require 'spec_helper'

feature 'Editar Plataforma de Bases de Datos' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:plataforma_bd)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Plataforma BD'
  end
  
  scenario 'Puedo editar una Plataforma de Base de Datos' do
    expect(page).to have_content("MySQL")
    click_link 'Editar'
    fill_in "Denominación", with: 'Oracle'
    click_button 'Actualizar Plataforma de Base de Datos'
    expect(page).to have_content("Oracle")
    expect(page).to have_content("Plataforma de Base de Datos modificada correctamente")
  end
  
  scenario "No puedo actualizar una Plataforma de Base de Datos sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Plataforma de Base de Datos"
    expect(page).to have_content("Hubo un error al modificar la Plataforma de Base de Datos")
  end
end