require 'spec_helper'

feature 'Ver Plataforma de Base de Datos' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todas las Plataformas de Base de Datos' do
    FactoryGirl.create(:plataforma_bd, denominacion: "MySQL")
    FactoryGirl.create(:plataforma_bd, denominacion: "Oracle")
    FactoryGirl.create(:plataforma_bd, denominacion: "SQL Server")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Plataforma BD'
    expect(page).to have_content("MySQL")
    expect(page).to have_content("Oracle")
    expect(page).to have_content("SQL Server")
  end
end