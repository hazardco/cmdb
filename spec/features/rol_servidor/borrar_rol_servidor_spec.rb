require 'spec_helper'

feature 'Borrar Rol de Servidor' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    base_datos = FactoryGirl.create(:rol_servidor, denominacion: "Base de Datos")
    visit '/'
    click_link 'Sistemas'
    click_link 'Roles de Servidor'
  end
  
  scenario 'Puedo borrar un Rol de Servidor' do
    expect(page).to have_content("Base de Datos")
    click_link 'Borrar'
    expect(page).to have_content("Rol de Servidor borrado correctamente")
  end

end