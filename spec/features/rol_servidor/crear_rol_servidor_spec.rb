require 'spec_helper'

feature 'Crear Rol de Servidor' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Sistemas'
    click_link 'Roles de Servidor'
    click_link 'Nuevo Rol de Servidor'
  end
  
  scenario 'Puedo crear un Rol de Servidor' do
    
    fill_in 'Denominación', with: 'Base de Datos'
    click_button 'Crear Rol de Servidor'
    
    expect(page).to have_content("Rol de Servidor añadido correctamente")
  end
  
  scenario "No puedo crear un Rol de Servidor sin una denominación" do
    
    fill_in 'Denominación', with: ''
    click_button 'Crear Rol de Servidor'
    
    expect(page).to have_content("Hubo un error al añadir el Rol de Servidor")
    
  end
end