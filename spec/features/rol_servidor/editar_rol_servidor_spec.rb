require 'spec_helper'

feature 'Editar Rol de Servidor' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    base_datos = FactoryGirl.create(:rol_servidor, denominacion: "Base de Datos")
    visit '/'
    click_link 'Sistemas'
    click_link 'Roles de Servidor'
  end
  
  scenario 'Puedo editar un Rol de Servidor' do
    expect(page).to have_content("Base de Datos")
    click_link 'Editar'
    fill_in "Denominación", with: 'Servidor DNS'
    click_button 'Actualizar Rol de Servidor'
    expect(page).to have_content("Servidor DNS")
    expect(page).to have_content("Rol de Servidor modificado correctamente")
  end
  
  scenario "No puedo actualizar un Rol de Servidor sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Rol de Servidor"
    expect(page).to have_content("Hubo un error al añadir el Rol de Servidor")
  end
end