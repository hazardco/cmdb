require 'spec_helper'

feature 'Ver Roles de Servidor' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
  scenario 'Puedo ver todos los Roles de Servidor' do
    base_datos = FactoryGirl.create(:rol_servidor, denominacion: "Base de Datos")
    dns = FactoryGirl.create(:rol_servidor, denominacion: "Servidor de DNS")
    visit '/'
    click_link 'Sistemas'
    click_link 'Roles de Servidor'
    expect(page).to have_content("Base de Datos")
    expect(page).to have_content("Servidor de DNS")
  end
end