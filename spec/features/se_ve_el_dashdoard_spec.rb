require 'spec_helper'

feature 'Se ve el Dashboard' do

  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end

  scenario 'El Dashboard se ve si se accede al root de la aplicación' do
    click_link 'Dashboard'
    expect(page).to have_content('Dashboard')
  end
  
  scenario 'La fecha es visible en español' do
    fecha = I18n.l Time.now, format: :long, locale: :es
    expect(page).to have_content(fecha)
  end
end