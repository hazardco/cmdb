require 'spec_helper'

feature 'Borrar Seguridad de Aplicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:seguridad_aplicacion)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Seguridad de Aplicación'
  end
  
  scenario 'Puedo borrar una Seguridad de Aplicación' do
    expect(page).to have_content("Base de Datos")
    click_link 'Borrar'
    expect(page).to have_content("Seguridad de Aplicación borrada correctamente")
  end
end