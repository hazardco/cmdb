require 'spec_helper'

feature 'Crear Seguridad de Aplicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Seguridad de Aplicación'
    click_link 'Nueva Seguridad de Aplicación'
  end
  
  scenario 'Puedo crear una Seguridad de Aplicación' do    
    fill_in 'Denominación', with: 'Base de Datos'
    click_button 'Crear Seguridad de Aplicación'
    
    expect(page).to have_content("Seguridad de la Aplicación añadida correctamente")
  end
  
  scenario "No puedo crear una Seguridad de Aplicación sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Seguridad de Aplicación'
    
    expect(page).to have_content("Hubo un error al añadir la Seguridad de la Aplicación")
    
  end
end