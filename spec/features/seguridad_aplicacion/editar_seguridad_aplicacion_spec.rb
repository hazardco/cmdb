require 'spec_helper'

feature 'Editar Seguridad de Aplicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:seguridad_aplicacion, denominacion: "Base de Datos")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Seguridad de Aplicación'
  end
  
  scenario 'Puedo editar una Seguridad de Aplicación' do
    expect(page).to have_content("Base de Datos")
    click_link 'Editar'
    fill_in "Denominación", with: 'LDAP'
    click_button 'Actualizar Seguridad de Aplicación'
    expect(page).to have_content("LDAP")
    expect(page).to have_content("Seguridad de Aplicación modificada correctamente")
  end
  
  scenario "No puedo actualizar una Seguridad de Aplicación sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Seguridad de Aplicación"
    expect(page).to have_content("Hubo un error al añadir la Seguridad de Aplicación")
  end
end