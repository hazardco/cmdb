require 'spec_helper'

feature 'Ver Seguridad de Aplicación' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos los tipos de Seguridad de Aplicación' do
    FactoryGirl.create(:seguridad_aplicacion, denominacion: "Base de Datos")
    FactoryGirl.create(:seguridad_aplicacion, denominacion: "LDAP")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Seguridad de Aplicación'
    expect(page).to have_content("Base de Datos")
    expect(page).to have_content("LDAP")
  end
end