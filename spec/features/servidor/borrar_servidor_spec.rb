require 'spec_helper'

feature 'Borrar Servidor' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:tipo_up)
    FactoryGirl.create(:plataforma)
    FactoryGirl.create(:familia)
    FactoryGirl.create(:idioma)
    FactoryGirl.create(:tipo_licencia)
    FactoryGirl.create(:entorno)
    FactoryGirl.create(:criticidad)
    FactoryGirl.create(:rol_servidor)
    FactoryGirl.create(:ubicacion)
    FactoryGirl.create(:servidor)
    visit '/'
    click_link 'Sistemas'
    click_link 'Servidores'
  end
  
  scenario 'Puedo borrar un Servidor' do
    expect(page).to have_content("dbmy01")
    click_link 'Borrar'
    expect(page).to have_content("Servidor borrado correctamente")
  end

end