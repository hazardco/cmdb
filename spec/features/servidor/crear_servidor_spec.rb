require "spec_helper"

feature "Crear Servidor" do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:tipo_up)
    FactoryGirl.create(:plataforma)
    FactoryGirl.create(:familia)
    FactoryGirl.create(:idioma)
    FactoryGirl.create(:tipo_licencia)
    FactoryGirl.create(:entorno)
    FactoryGirl.create(:criticidad)
    FactoryGirl.create(:rol_servidor)
    FactoryGirl.create(:ubicacion)
    
    visit '/'
    click_link 'Sistemas'
    click_link 'Servidores'
    click_link 'Nuevo Servidor'
  end
  scenario "Puedo Crear un Servidor" do
    fill_in 'Hostname', with: "dbmy01"
    fill_in 'FQDN', with: "dbmy01.eco.pri"
    fill_in "Máquina Virtual", with: "dbmy01vir"
    fill_in "Número de Licencias", with: "23"
    fill_in "Descripción", with: "Servidor de Bases de datos"
    fill_in "Memoria", with: "8"
    fill_in "Número de Procesadores", with: "4"
    fill_in "Tipo de Procesador", with: "Intelx64"
    fill_in "Rack", with: "2763F"
    fill_in "Marca", with: "Dell"
    fill_in "Modelo", with: "Opterom 343"
    fill_in "Número de Serie", with: "12311FFg234"
    fill_in "Tamaño(U)", with: "1"
    fill_in "Conexiones", with: "2"
    fill_in "Tipo de Conexiones", with: "RJ45"
    fill_in "Intensidad", with: "45"
    fill_in "Potencia Máxima", with: "300"
    click_button 'Crear Servidor'
    expect(page).to have_content("Servidor añadido correctamente")
  end
  
  scenario "No puedo Crear un Servidor sin descripción" do
    fill_in 'Hostname', with: "dbmy01"
    fill_in 'FQDN', with: "dbmy01.eco.pri"
    fill_in "Máquina Virtual", with: "dbmy01vir"
    fill_in "Número de Licencias", with: "23"
    fill_in "Descripción", with: ""
    fill_in "Memoria", with: "8"
    fill_in "Número de Procesadores", with: "4"
    fill_in "Tipo de Procesador", with: "Intelx64"
    fill_in "Rack", with: "2763F"
    fill_in "Marca", with: "Dell"
    fill_in "Modelo", with: "Opterom 343"
    fill_in "Número de Serie", with: "12311FFg234"
    fill_in "Tamaño(U)", with: "1"
    fill_in "Conexiones", with: "2"
    fill_in "Tipo de Conexiones", with: "RJ45"
    fill_in "Intensidad", with: "45"
    fill_in "Potencia Máxima", with: "300"
    click_button 'Crear Servidor'
    expect(page).to have_content("Hubo un error al añadir el Servidor")
  end
end