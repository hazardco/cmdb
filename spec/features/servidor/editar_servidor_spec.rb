require 'spec_helper'

feature 'Editar Servidor' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:tipo_up)
    FactoryGirl.create(:plataforma)
    FactoryGirl.create(:familia)
    FactoryGirl.create(:idioma)
    FactoryGirl.create(:tipo_licencia)
    FactoryGirl.create(:entorno)
    FactoryGirl.create(:criticidad)
    FactoryGirl.create(:rol_servidor)
    FactoryGirl.create(:ubicacion)
    FactoryGirl.create(:servidor)
    visit '/'
    click_link 'Sistemas'
    click_link 'Servidores'
  end
  
  scenario 'Puedo editar un Servidor' do
    expect(page).to have_content("dbmy01")
    click_link 'Editar'
    fill_in "Hostname", with: 'dbmy02'
    click_button 'Actualizar Servidor'
    expect(page).to have_content("dbmy02")
    expect(page).to have_content("Servidor modificado correctamente")
  end
  
  scenario "No puedo actualizar un Servidor sin denominación" do
    click_link "Editar"
    fill_in "Descripción", with: ""
    click_button "Actualizar Servidor"
    expect(page).to have_content("Hubo un error al añadir el Servidor")
  end
end