require 'spec_helper'

feature 'Ver Servidores' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
  scenario 'Puedo ver todos los Idiomas' do
    FactoryGirl.create(:tipo_up)
    FactoryGirl.create(:plataforma)
    FactoryGirl.create(:familia)
    FactoryGirl.create(:idioma)
    FactoryGirl.create(:tipo_licencia)
    FactoryGirl.create(:entorno)
    FactoryGirl.create(:criticidad)
    FactoryGirl.create(:rol_servidor)
    FactoryGirl.create(:ubicacion)
    FactoryGirl.create(:servidor)
    
    visit '/'
    click_link 'Sistemas'
    click_link 'Servidores'
    expect(page).to have_content("dbmy01")

  end
end