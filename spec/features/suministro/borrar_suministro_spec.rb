require 'spec_helper'

feature 'Borrar Suministro' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    lenguaje = FactoryGirl.create(:suministro)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Suministros'
  end
  
  scenario 'Puedo borrar un Suministro' do
    expect(page).to have_content("Interno a medida")
    click_link 'Borrar'
    expect(page).to have_content("Suministro borrado correctamente")
  end
end