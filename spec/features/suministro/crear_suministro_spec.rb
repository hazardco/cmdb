require 'spec_helper'

feature 'Crear Suministro' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Suministros'
    click_link 'Nuevo Suministro'
  end
  
  scenario 'Puedo crear un Suministro' do    
    fill_in 'Denominación', with: 'Interno a medida'
    click_button 'Crear Suministro'
    
    expect(page).to have_content("Suministro añadido correctamente")
  end
  
  scenario "No puedo crear un Suministro sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Suministro'
    
    expect(page).to have_content("Hubo un error al añadir el Suministro")
    
  end
end