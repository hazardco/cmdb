require 'spec_helper'

feature 'Editar Suministro' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    ruby = FactoryGirl.create(:suministro, denominacion: "Interno a medida")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Suministros'
  end
  
  scenario 'Puedo editar un Suministro' do
    expect(page).to have_content("Interno a medida")
    click_link 'Editar'
    fill_in "Denominación", with: 'Externo'
    click_button 'Actualizar Suministro'
    expect(page).to have_content("Externo")
    expect(page).to have_content("Suministro modificado correctamente")
  end
  
  scenario "No puedo actualizar un Suministro sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Suministro"
    expect(page).to have_content("Hubo un error al añadir el Suministro")
  end
end