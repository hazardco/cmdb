require 'spec_helper'

feature 'Ver Suministros' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos las Suministros' do
    FactoryGirl.create(:suministro, denominacion: "Interno a medida")
    FactoryGirl.create(:suministro, denominacion: "Externo")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Suministros'
    expect(page).to have_content("Interno a medida")
    expect(page).to have_content("Externo")
  end
end