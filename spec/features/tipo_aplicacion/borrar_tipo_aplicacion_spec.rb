require 'spec_helper'

feature 'Borrar Tipo de Aplicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:tipo_aplicacion)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Tipos de Aplicación'
  end
  
  scenario 'Puedo borrar un Tipo de Aplicación' do
    expect(page).to have_content("Desarrollo")
    click_link 'Borrar'
    expect(page).to have_content("Tipo de la Aplicación borrado correctamente")
  end
end