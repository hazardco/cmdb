require 'spec_helper'

feature 'Crear Tipo de Aplicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Tipos de Aplicación'
    click_link 'Nuevo Tipo de Aplicación'
  end
  
  scenario 'Puedo crear un Tipo de Aplicación' do    
    fill_in 'Denominación', with: 'Aplicación Web'
    click_button 'Crear Tipo de Aplicación'
    
    expect(page).to have_content("Tipo de la Aplicación añadido correctamente")
  end
  
  scenario "No puedo crear un Estado de Aplicación sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Tipo de Aplicación'
    
    expect(page).to have_content("Hubo un error al añadir el Tipo de la Aplicación")
    
  end
end