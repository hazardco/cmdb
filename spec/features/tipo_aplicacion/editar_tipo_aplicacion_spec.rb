require 'spec_helper'

feature 'Editar Tipo de Aplicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:tipo_aplicacion, denominacion: "Aplicación Web")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Tipos de Aplicación'
  end
  
  scenario 'Puedo editar un Tipo de Aplicación' do
    expect(page).to have_content("Aplicación Web")
    click_link 'Editar'
    fill_in "Denominación", with: 'Escritorio'
    click_button 'Actualizar Tipo de Aplicación'
    expect(page).to have_content("Escritorio")
    expect(page).to have_content("Tipo de la Aplicación modificado correctamente")
  end
  
  scenario "No puedo actualizar un Tipo de Aplicación sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Tipo de Aplicación"
    expect(page).to have_content("Hubo un error al añadir el Tipo de la Aplicación")
  end
end