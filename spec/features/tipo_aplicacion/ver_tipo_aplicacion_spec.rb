require 'spec_helper'

feature 'Ver Tipos de Aplicación' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos los Tipos de Aplicación' do
    FactoryGirl.create(:tipo_aplicacion, denominacion: "Aplicación web")
    FactoryGirl.create(:tipo_aplicacion, denominacion: "Escritorio")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Tipos de Aplicación'
    expect(page).to have_content("Aplicación web")
    expect(page).to have_content("Escritorio")
  end
end