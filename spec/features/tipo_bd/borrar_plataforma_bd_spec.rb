require 'spec_helper'

feature 'Borrar Tipo de Base de Datos' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:tipo_bd)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Tipo BD'
  end
  
  scenario 'Puedo borrar un Tipo de Base de Datos' do
    expect(page).to have_content("Relacional")
    click_link 'Borrar'
    expect(page).to have_content("Tipo de Base de Datos borrada correctamente")
  end
end