require 'spec_helper'

feature 'Crear Tipo de Base de Datos' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Tipo BD'
    click_link 'Nuevo Tipo de Base de Datos'
  end
  
  scenario 'Puedo crear un Tipo de Base de Datos' do    
    fill_in 'Denominación', with: 'Relacional'
    click_button 'Crear Tipo de Base de Datos'
    
    expect(page).to have_content("Tipo de Base de Datos añadida correctamente")
  end
  
  scenario "No puedo crear un Tipo de Base de Datos sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Tipo de Base de Datos'
    
    expect(page).to have_content("Hubo un error al añadir el Tipo de Base de Datos")
    
  end
end