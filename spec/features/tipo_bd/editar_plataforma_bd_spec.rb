require 'spec_helper'

feature 'Editar Tipo de Base de Datos' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    FactoryGirl.create(:tipo_bd)
    visit '/'
    click_link 'Desarrollo'
    click_link 'Tipo BD'
  end
  
  scenario 'Puedo editar un Tipo de Base de Datos' do
    expect(page).to have_content("Relacional")
    click_link 'Editar'
    fill_in "Denominación", with: 'Documental'
    click_button 'Actualizar Tipo de Base de Datos'
    expect(page).to have_content("Documental")
    expect(page).to have_content("Tipo de Base de Datos modificada correctamente")
  end
  
  scenario "No puedo actualizar un Tipo de Base de Datos sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Tipo de Base de Datos"
    expect(page).to have_content("Hubo un error al modificar el Tipo de Base de Datos")
  end
end