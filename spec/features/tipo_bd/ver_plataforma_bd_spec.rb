require 'spec_helper'

feature 'Ver Tipo de Base de Datos' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos los Tipos de Base de Datos' do
    FactoryGirl.create(:tipo_bd, denominacion: "Relacional")
    FactoryGirl.create(:tipo_bd, denominacion: "Documental")
    FactoryGirl.create(:tipo_bd, denominacion: "NoSQL")
    visit '/'
    click_link 'Desarrollo'
    click_link 'Tipo BD'
    expect(page).to have_content("Relacional")
    expect(page).to have_content("Documental")
    expect(page).to have_content("NoSQL")
  end
end