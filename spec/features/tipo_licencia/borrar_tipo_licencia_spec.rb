require 'spec_helper'

feature 'Borrar Tipo de Licencias' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    windows = FactoryGirl.create(:tipo_licencia, denominacion: "Libre")
    visit '/'
    click_link 'Sistemas'
    click_link 'Tipos de Licencia'
  end
  
  scenario 'Puedo borrar un Tipo de Licencia' do
    expect(page).to have_content("Libre")
    click_link 'Borrar'
    expect(page).to have_content("Tipo de Licencia borrado correctamente")
  end

end