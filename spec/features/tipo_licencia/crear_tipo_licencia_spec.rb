require 'spec_helper'

feature 'Crear Tipo de Licencia' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Sistemas'
    click_link 'Tipos de Licencia'
    click_link 'Nuevo Tipo de Licencia'
  end
  scenario 'Puedo crear un Tipo de Licencia' do
    fill_in 'Denominación', with: 'Libre'
    click_button 'Crear Tipo de Licencia'
    
    expect(page).to have_content("Tipo de Licencia añadido correctamente")
  end
  
  scenario "No puedo crear un Tipo de Licencia sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Tipo de Licencia'
    
    expect(page).to have_content("Hubo un error al añadir el Tipo de Licencia")
    
  end
end