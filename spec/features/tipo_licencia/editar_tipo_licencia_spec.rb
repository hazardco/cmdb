require 'spec_helper'

feature 'Editar Tipo de Licencia' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    libre = FactoryGirl.create(:tipo_licencia, denominacion: "Libre")
    visit '/'
    click_link 'Sistemas'
    click_link 'Tipos de Licencia'
  end
  
  scenario 'Puedo editar un Tipo de Licencia' do
    expect(page).to have_content("Libre")
    click_link 'Editar'
    fill_in "Denominación", with: 'Propietaria'
    click_button 'Actualizar Tipo de Licencia'
    expect(page).to have_content("Propietaria")
    expect(page).to have_content("Tipo de Licencia modificado correctamente")
  end
  
  scenario "No puedo actualizar un Tipo de Licencia sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Tipo de Licencia"
    expect(page).to have_content("Hubo un error al añadir el Tipo de Licencia")
  end
end