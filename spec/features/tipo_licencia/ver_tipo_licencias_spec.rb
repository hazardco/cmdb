require 'spec_helper'

feature 'Ver Tipos de Licencia' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
  scenario 'Puedo ver todos los Tipos de Licencia' do
    libre = FactoryGirl.create(:tipo_licencia, denominacion: "Libre")
    propietaria = FactoryGirl.create(:tipo_licencia, denominacion: "Propietaria")
    shareware = FactoryGirl.create(:tipo_licencia, denominacion: "Shareware")

    visit '/'
    click_link 'Sistemas'
    click_link 'Tipos de Licencia'
    expect(page).to have_content("Libre")
    expect(page).to have_content("Propietaria")
    expect(page).to have_content("Shareware")
  end
end