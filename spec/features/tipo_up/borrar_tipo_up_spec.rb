require 'spec_helper'

feature 'Borrar Tipo de Unidad de Proceso' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    windows = FactoryGirl.create(:tipo_up, denominacion: "Virtual")
    visit '/'
    click_link 'Sistemas'
    click_link 'Tipos de UP'
  end
  
  scenario 'Puedo borrar un Tipo de Unidad de Procesos' do
    expect(page).to have_content("Virtual")
    click_link 'Borrar'
    expect(page).to have_content("Tipo de UP borrado correctamente")
  end

end