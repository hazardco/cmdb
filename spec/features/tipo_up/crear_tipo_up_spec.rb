require 'spec_helper'

feature 'Crear Tipo de Unidad de Proceso' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Sistemas'
    click_link 'Tipos de UP'
    click_link 'Nuevo Tipo de UP'
  end
  scenario 'Puedo crear un Tipo de Unidad de Proceso' do
    
    fill_in 'Denominación', with: 'Virtual'
    click_button 'Crear Tipo de UP'
    
    expect(page).to have_content("Tipo de UP añadido correctamente")
  end
  
  scenario "No puedo crear un Entorno sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Tipo de UP'
    
    expect(page).to have_content("Hubo un error al añadir el Tipo de UP")
    
  end
end