require 'spec_helper'

feature 'Editar Tipo de Unidad de Proceso' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    windows = FactoryGirl.create(:tipo_up, denominacion: "Virtual")
    visit '/'
    click_link 'Sistemas'
    click_link 'Tipos de UP'
  end
  
  scenario 'Puedo editar un Tipo de Unidad de Proceso' do
    expect(page).to have_content("Virtual")
    click_link 'Editar'
    fill_in "Denominación", with: 'Físico'
    click_button 'Actualizar Tipo de UP'
    expect(page).to have_content("Físico")
    expect(page).to have_content("Tipo de UP modificado correctamente")
  end
  
  scenario "No puedo actualizar un Tipo de UP sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Tipo de UP"
    expect(page).to have_content("Hubo un error al añadir el Tipo de UP")
  end
end