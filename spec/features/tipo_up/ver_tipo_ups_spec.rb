require 'spec_helper'

feature 'Ver Tipos de UP' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
  scenario 'Puedo ver todos los Tipos de UP' do
    virtual = FactoryGirl.create(:tipo_up, denominacion: "Virtual")
    fisico = FactoryGirl.create(:tipo_up, denominacion: "Físico")
    visit '/'
    click_link 'Sistemas'
    click_link 'Tipos de UP'
    expect(page).to have_content("Virtual")
    expect(page).to have_content("Físico")
  end
end