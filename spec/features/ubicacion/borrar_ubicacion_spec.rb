require 'spec_helper'

feature 'Borrar Ubicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    windows = FactoryGirl.create(:ubicacion, denominacion: "CPD Morerías")
    visit '/'
    click_link 'Sistemas'
    click_link 'Ubicaciones'
  end
  
  scenario 'Puedo borrar una Ubicación' do
    expect(page).to have_content("CPD Morerías")
    click_link 'Borrar'
    expect(page).to have_content("Ubicación borrado correctamente")
  end

end