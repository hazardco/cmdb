require 'spec_helper'

feature 'Crear Ubicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Sistemas'
    click_link 'Ubicaciones'
    click_link 'Nueva Ubicación'
  end
  
  scenario 'Puedo crear una ubicación' do
    fill_in 'Denominación', with: 'CPD Morerías'
    click_button 'Crear Ubicación'
    
    expect(page).to have_content("Ubicación añadida correctamente")
  end
  
  scenario "No puedo crear una Ubicación sin una denominación" do
    fill_in 'Denominación', with: ''
    click_button 'Crear Ubicación'
    
    expect(page).to have_content("Hubo un error al añadir la Ubicación")
    
  end
end