require 'spec_helper'

feature 'Editar Ubicación' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    windows = FactoryGirl.create(:ubicacion, denominacion: "CPD Morerías")
    visit '/'
    click_link 'Sistemas'
    click_link 'Ubicaciones'
  end
  
  scenario 'Puedo editar una Ubicación' do
    expect(page).to have_content("CPD Morerías")
    click_link 'Editar'
    fill_in "Denominación", with: 'La nube'
    click_button 'Actualizar Ubicación'
    expect(page).to have_content("La nube")
    expect(page).to have_content("Ubicación modificada correctamente")
  end
  
  scenario "No puedo actualizar una Ubicación sin denominación" do
    click_link "Editar"
    fill_in "Denominación", with: ""
    click_button "Actualizar Ubicación"
    expect(page).to have_content("Hubo un error al añadir la Ubicación")
  end
end