require 'spec_helper'

feature 'Ver Entornos' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
  scenario 'Puedo ver todos los Entornos' do
    morerias = FactoryGirl.create(:ubicacion, denominacion: "CPD Morerías")
    nube = FactoryGirl.create(:ubicacion, denominacion: "La nube")
    educacion = FactoryGirl.create(:ubicacion, denominacion: "CPD Educación")
    visit '/'
    click_link 'Sistemas'
    click_link 'Ubicaciones'
    expect(page).to have_content("CPD Morerías")
    expect(page).to have_content("La nube")
    expect(page).to have_content("CPD Educación")
  end
end