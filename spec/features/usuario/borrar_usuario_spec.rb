require 'spec_helper'

feature 'Borrar Entorno' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    visit '/'
    click_link 'Administración'
    click_link 'Usuarios'
  end
  
  scenario 'Puedo borrar un Usuario' do
    expect(page).to have_content("Luis Miguel")
    click_link 'Borrar'
    expect(page).to have_content("Usuario borrado correctamente")
  end

end