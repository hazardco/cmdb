require 'spec_helper'

feature 'Crear Usuario' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    visit '/'
    click_link 'Administración'
    click_link 'Usuarios'
    click_link 'Nuevo Usuario'
  end
  
  scenario 'Puedo crear un usuario' do    
    fill_in 'Nombre', with: 'Antonio'
    fill_in 'Apellidos', with: 'Zorita'
    fill_in 'Usuario', with: 'antonio.zorita'
    fill_in 'Correo', with: 'antonio.zorita@gobex.es'
    fill_in 'Password', with: 'azorita'
    fill_in 'Confirma la Password', with: 'azorita'

    click_button 'Crear Usuario'
    
    expect(page).to have_content("Usuario añadido correctamente")
  end
  
  scenario "No puedo crear un Usuario sin nombre ni apellidos" do
    fill_in 'Nombre', with: ''
    fill_in 'Apellidos', with: ''
    fill_in 'Usuario', with: ''
    fill_in 'Correo', with: ''
    fill_in 'Password', with: 'azorita'
    fill_in 'Confirma la Password', with: 'azorita'    
    click_button 'Crear Usuario'
    
    expect(page).to have_content("Hubo un error al añadir el Usuario")
    
  end
end