require 'spec_helper'

feature 'Editar Usuario' do
  
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
    
    visit '/'
    click_link 'Administración'
    click_link 'Usuarios'
  end
  
  scenario 'Puedo editar un Usuario' do
    expect(page).to have_content("Luis Miguel")
    click_link 'Editar'
    fill_in "Apellidos", with: 'Cabezas Jaramillo'
    click_button 'Actualizar Usuario'
    expect(page).to have_content("Cabezas Jaramillo")
    expect(page).to have_content("Usuario modificado correctamente")
  end
  
  scenario "No puedo actualizar un Usuario sin nombre ni apellidos" do
    click_link "Editar"
    fill_in "Apellidos", with: ""
    click_button "Actualizar Usuario"
    expect(page).to have_content("Hubo un error al añadir el Usuario")
  end
end