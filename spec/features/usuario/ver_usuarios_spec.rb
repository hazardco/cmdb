require 'spec_helper'

feature 'Ver Usuarios' do
  before do
    usuario = FactoryGirl.create(:usuario)
    autenticar_como!(usuario)
  end
      
  scenario 'Puedo ver todos los Usuarios' do
    visit '/'
    click_link 'Administración'
    click_link 'Usuarios'
    expect(page).to have_content("Luis Miguel")
    expect(page).to have_content("Cabezas Granado")
  end
end