require 'spec_helper'
describe Usuario do
  describe "passwords" do
    it "Es necesario una password y un password confirmado para poder grabar el usuario" do
      u = Usuario.new(nombre: "Luis Miguel")
      u.save
      expect(u).to_not be_valid
      u.password = "password"
      u.password_confirmation = ""
      u.save
      expect(u).to_not be_valid
      u.password_confirmation = "password"
      u.save
      expect(u).to be_valid
    end
    
    it "Es necesario un password y una confirmación" do
      u = Usuario.create(
        nombre: "Luis Miguel",
        password: "password",
        password_confirmation: "password2")
      expect(u).to_not be_valid
    end
    
    
  end
end
