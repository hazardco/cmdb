module AutenticacionHelpers
  def autenticar_como!(usuario)
    visit '/login'
    fill_in "Usuario", with: usuario.usuario
    fill_in "Password", with: usuario.password
    click_button 'Entrar'
    expect(page).to have_content("Login correcto")
  end 
end
RSpec.configure do |c|
  c.include AutenticacionHelpers, type: :feature
end